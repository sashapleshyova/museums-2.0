import doc1 from './img/documentation/000 Р96 Оп2 Д1.jpg'
import doc2 from './img/documentation/001.jpg'
import doc3 from './img/documentation/001 12.11.1918 - Утверждение президиума Исполкома и распред должностей.JPG'
import doc4 from './img/documentation/002 19.11.1918 - Чистка от беспартийных.JPG'
import doc5 from './img/documentation/003  Производственный план Кунеевской школы в 1928-29 01.jpg'
import doc6 from './img/documentation/003 20.11.1918 - Выборы председателя.jpg'
import doc7 from './img/documentation/004 +01 ЗавНаробразом назначен Ингельберг.jpg'
import doc8 from './img/documentation/004  Производственный план Кунеевской школы в 1928-29 02.jpg'
import doc9 from './img/documentation/005 +02.jpg'
import doc10 from './img/documentation/006 Список лиц на важных должностях в 1918г.jpg'
import doc11 from './img/documentation/006  Производственный план Кунеевской школы в 1928-29 04.jpg'
import doc12 from './img/documentation/007 +01.jpg'
import doc13 from './img/documentation/008  Производственный план Подборинской школы в 1928-29 02-1.jpg'
import doc14 from './img/documentation/008  Производственный план Подборинской школы в 1928-29 02.jpg'
import doc15 from './img/documentation/009 +03.jpg'
import doc16 from './img/documentation/009  Производственный план Подборинской школы в 1928-29 03.jpg'
import doc17 from './img/documentation/010 +04.jpg'
import doc18 from './img/documentation/010  Производственный план Подборинской школы в 1928-29 04.jpg'
import doc19 from './img/documentation/011  Производственный план Дворяновской школы в 1928-29 01.jpg'
import doc20 from './img/documentation/012 +06.jpg'
import doc21 from './img/documentation/012  Производственный план Дворяновской школы в 1928-29 02.jpg'
import doc22 from './img/documentation/013 Список служащих Ставр ГорОНО в 1918г.jpg'
import doc23 from './img/documentation/013  Производственный план Дворяновской школы в 1928-29 03.jpg'
import doc24 from './img/documentation/014 +01.jpg'
import doc25 from './img/documentation/014  Производственный план Морквашинской школы в 1928-29 01.jpg'
import doc26 from './img/documentation/015 +02.jpg'
import doc27 from './img/documentation/016 +03.jpg'
import doc28 from './img/documentation/016  Производственный план Морквашинской школы в 1928-29 03.jpg'
import doc29 from './img/documentation/017 +04.jpg'
import doc30 from './img/documentation/017  Производственный план Морквашинской школы в 1928-29 04.jpg'
import doc31 from './img/documentation/018 +05.jpg'
import doc32 from './img/documentation/018  Производственный план Морквашинской школы в 1928-29 05.jpg'
import doc33 from './img/documentation/019  Производственный план Морквашинской школы в 1928-29 06.jpg'
import doc34
    from './img/documentation/019 01.06.1919 Создание Уезда.jpg'
import doc35 from './img/documentation/020 +01.jpg'
import doc36 from './img/documentation/021 +02.jpg'
import doc37 from './img/documentation/021  Производственный план Морквашинской школы в 1928-29 08.jpg'
import doc38 from './img/documentation/022 Р150 Оп1 Д1а Протоколы ГорОНО.jpg'
import doc39 from './img/documentation/022  Производственный план Морквашинской школы в 1928-29 09.jpg'
import doc40 from './img/documentation/023  Производственный план Морквашинской школы в 1928-29 10.jpg'
import doc41 from './img/documentation/024  Производственный план Хрящевской школы №2 в 1928-29 01.jpg'
import doc42
    from './img/documentation/025 Засед школьных врачей г Ставрополя 19.06.1918 - ПЕРВЫЕ шаги к здоровью учеников.jpg'
import doc43 from './img/documentation/025  Производственный план Хрящевской школы №2 в 1928-29 02.jpg'
import doc44 from './img/documentation/026 Заседание медотдела - девочкам дать фельдшера-женщину.jpg'
import doc45 from './img/documentation/026  Производственный план Хрящевской школы №2 в 1928-29 03.jpg'
import doc46 from './img/documentation/027 Протокол - педагогов нет вводятся одногодичные курсы для ЕТШ.jpg'
import doc47 from './img/documentation/027  Производственный план Хрящевской школы №2 в 1928-29 04.jpg'
import doc48 from './img/documentation/028 +01.jpg'
import doc49 from './img/documentation/028  Производственный план Хрящевской школы №2 в 1928-29 05-1.jpg'
import doc50 from './img/documentation/028  Производственный план Хрящевской школы №2 в 1928-29 05.jpg'
import doc51 from './img/documentation/029 Зав. Горанский - подготовка к ЕТШ-1.jpg'
import doc52 from './img/documentation/029 Зав. Горанский - подготовка к ЕТШ.jpg'
import doc53 from './img/documentation/029  Производственный план Хрящевской школы №2 в 1928-29 06.jpg'
import doc54 from './img/documentation/030 +01.jpg'
import doc55 from './img/documentation/030  Производственный план Хрящевской школы №2 в 1928-29 07.jpg'
import doc56
    from './img/documentation/031 Протокол 11.12.1918 - закрытие женской гимназии и открытие в ней учительской семинарии.jpg'
import doc57 from './img/documentation/031  Производственный план Хрящевской школы №2 в 1928-29 08.jpg'
import doc58
    from './img/documentation/032 +01 Запрет учить священослужителям + наём водовоза для школ + неотъём учебных помещений под другие цели.jpg'
import doc59 from './img/documentation/032  Производственный план Хрящевской школы №2 в 1928-29 09.jpg'
import doc60 from './img/documentation/033  Производственный план Хрящевской школы №2 в 1928-29 10.jpg'
import doc61 from './img/documentation/034 +02 Столовая для голодающих учеников.jpg'
import doc62 from './img/documentation/034  Производственный план Хрящевской школы №2 в 1928-29 11.jpg'
import doc63 from './img/documentation/035 +03.jpg'
import doc64 from './img/documentation/035  Производственный план Хрящевской школы №2 в 1928-29 12.jpg'
import doc65 from './img/documentation/036 Протокол 14.12.1918 - Создание родкомитетов.jpg'
import doc66 from './img/documentation/036  Производственный план Хрящевской школы №2 в 1928-29 13.jpg'
import doc67 from './img/documentation/037 Протокол 18.01.1919 - о воссоздании ремесленых училищ.jpg'
import doc68 from './img/documentation/037  Производственный план Хрящевской школы №2 в 1928-29 14.jpg'
import doc69 from './img/documentation/038 +01.jpg'
import doc70 from './img/documentation/038  Производственный план Хрящевской школы №2 в 1928-29 15.jpg'
import doc71 from './img/documentation/039 28.12.1918 - Открытие школы красноармейцев со 2 января.jpg'
import doc72 from './img/documentation/040 Увольнение.jpg'
import doc73 from './img/documentation/041  Производственный план Хрящевской школы №2 в 1928-29 18.jpg'
import doc74 from './img/documentation/041 19.05.1919 - Реорг школьных советов.jpg'
import doc75 from './img/documentation/042 +01 Оставить работникам обра дачи.jpg'
import doc76 from './img/documentation/042  Производственный план Хрящевской школы №2 в 1928-29 19.jpg'
import doc77 from './img/documentation/043 23.05.1919 Выдача ученикам мыла.jpg'
import doc78 from './img/documentation/045 +01.jpg'
import doc79
    from './img/documentation/046 +02 Поездка Горанского в Мелекесс за одеждой и обувью + создание подотдела Снабжения.jpg'
import doc80 from './img/documentation/047 26.05.1919 - назначен первый субботник.jpg'
import doc81 from './img/documentation/048 +01 Сбор лекарственных трав и цветов учениками.jpg'
import doc82 from './img/documentation/049 27.05.1919.jpg'
import doc83 from './img/documentation/050 28.05.1919 Первая изба читальня - назначен заведующий.jpg'
import doc84 from './img/documentation/051 30.05.1919 Городской сад стал детской площадкой.jpg'
import doc85 from './img/documentation/052 +01 Перепись грамотных.jpg'
import doc86 from './img/documentation/053 Возвращение Горанского.jpg'
import doc87 from './img/documentation/129 План уроков с темой Наступление весны 01.jpg'
import doc88 from './img/documentation/130 План уроков с темой Наступление весны 02.jpg'
import doc89 from './img/documentation/131 План уроков с темой Наступление весны 03.jpg'
import doc90 from './img/documentation/132 План уроков с темой Наступление весны 04.jpg'
import doc91 from './img/documentation/133 План Ташёлской школы 1й ступени 01.jpg'
import doc92 from './img/documentation/134 План Ташёлской школы 1й ступени 02.jpg'
import doc93 from './img/documentation/135 План Ташёлской школы 1й ступени 03.jpg'
import doc94 from './img/documentation/136 План Ташёлской школы 1й ступени 04.jpg'
import doc95 from './img/documentation/137 План Ташёлской школы 1й ступени 05.jpg'
import doc96 from './img/documentation/138 План Ташёлской школы 1й ступени 06.jpg'
import doc97 from './img/documentation/139 План Ташёлской школы 1й ступени 07.jpg'
import doc98 from './img/documentation/140 План Ташёлской школы 1й ступени 08.jpg'
import doc99 from './img/documentation/141 План Ташёлской школы 1й ступени 09.jpg'
import doc100 from './img/documentation/142 План Ташёлской школы 1й ступени 10.jpg'
import doc101 from './img/documentation/aaa DSC03614.JPG'
import doc102 from './img/documentation/aaa DSC03615.JPG'
import doc103 from './img/documentation/aaa DSC03617.JPG'
import doc104 from './img/documentation/aaa DSC03618.JPG'
import doc105 from './img/documentation/aaa DSC03619-1.JPG'
import doc106 from './img/documentation/aaa DSC03619.JPG'
import doc107 from './img/documentation/aaa DSC03620.JPG'
import doc108 from './img/documentation/aaa DSC03623.JPG'
import doc109 from './img/documentation/aaa DSC03624.JPG'
import doc110 from './img/documentation/aaa DSC03626.JPG'
import doc111 from './img/documentation/aaa DSC03627.JPG'
import doc112 from './img/documentation/aaa DSC03628.JPG'
import doc113 from './img/documentation/aaa DSC03632.JPG'
import doc114 from './img/documentation/aaa DSC03633.JPG'
import doc115 from './img/documentation/aaa DSC03637.JPG'
import doc116 from './img/documentation/aaa DSC03639.JPG'
import doc117 from './img/documentation/aaa DSC03642.JPG'
import doc118 from './img/documentation/aaa DSC03644.JPG'
import doc119 from './img/documentation/aaa DSC03645.JPG'
import doc120 from './img/documentation/aaa DSC03647.JPG'
import doc121 from './img/documentation/aaa DSC03648.JPG'
import doc122 from './img/documentation/aaa IMG_5876.jpg'
import doc123 from './img/documentation/aaa IMG_5877.jpg'


export default [
    {
        img: doc1,
        text: {
            RU: "",
            EN: ""
        }
    },
    {
        img: doc2,
        text: {
            RU: "",
            EN: ""
        }
    },
    {
        img: doc3,
        text: {
            RU: "Утверждение президиума Исполкома и распред должностей",
            EN: ""
        }
    },
    {
        img: doc4,
        text: {
            RU: "Чистка от беспартийных",
            EN: ""
        }
    },
    {
        img: doc6,
        text: {
            RU: "Выборы председателя",
            EN: ""
        }
    },
    {
        img: doc7,
        text: {
            RU: "ЗавНаробразом назначен Ингельберг",
            EN: ""
        }
    },
    {
        img: doc5,
        text: {
            RU: "Производственный план Кунеевской школы в 1928-29",
            EN: ""
        }
    },
    {
        img: doc8,
        text: {
            RU: "Производственный план Кунеевской школы",
            EN: ""
        }
    },
    {
        img: doc11,
        text: {
            RU: "Производственный план Кунеевской школы в 1928-29",
            EN: ""
        }
    },
    {
        img: doc10,
        text: {
            RU: "Список лиц на важных должностях в 1918г",
            EN: ""
        }
    },
    {
        img: doc9,
        text: {
            RU: "",
            EN: ""
        }
    },
    {
        img: doc12,
        text: {
            RU: "",
            EN: ""
        }
    },
    {
        img: doc13,
        text: {
            RU: "Производственный план Подборинской школы в 1928-29",
            EN: ""
        }
    },
    {
        img: doc14,
        text: {
            RU: "Производственный план Подборинской школы в 1928-29",
            EN: ""
        }
    },
    {
        img: doc16,
        text: {
            RU: "Производственный план Подборинской школы в 1928-29",
            EN: ""
        }
    },
    {
        img: doc18,
        text: {
            RU: "Производственный план Подборинской школы в 1928-29",
            EN: ""
        }
    },
    {
        img: doc15,
        text: {
            RU: "",
            EN: ""
        }
    },
    {
        img: doc17,
        text: {
            RU: "",
            EN: ""
        }
    },
    {
        img: doc19,
        text: {
            RU: "Производственный план Дворяновской школы в 1928-29",
            EN: ""
        }
    },
    {
        img: doc21,
        text: {
            RU: "Производственный план Дворяновской школы в 1928-29",
            EN: ""
        }
    },
    {
        img: doc23,
        text: {
            RU: "Производственный план Дворяновской школы в 1928-29",
            EN: ""
        }
    },
    {
        img: doc20,
        text: {
            RU: "",
            EN: ""
        }
    },
    {
        img: doc22,
        text: {
            RU: "Список служащих Ставр ГорОНО в 1918г.",
            EN: ""
        }
    },
    {
        img: doc24,
        text: {
            RU: "",
            EN: ""
        }
    },
    {
        img: doc25,
        text: {
            RU: "Производственный план Морквашинской школы в 1928-29",
            EN: ""
        }
    },
    {
        img: doc28,
        text: {
            RU: "Производственный план Морквашинской школы в 1928-29",
            EN: ""
        }
    },
    {
        img: doc30,
        text: {
            RU: "Производственный план Морквашинской школы в 1928-29",
            EN: ""
        }
    },
    {
        img: doc32,
        text: {
            RU: "Производственный план Морквашинской школы в 1928-29",
            EN: ""
        }
    },
    {
        img: doc33,
        text: {
            RU: "Производственный план Морквашинской школы в 1928-29",
            EN: ""
        }
    },
    {
        img: doc37,
        text: {
            RU: "Производственный план Морквашинской школы в 1928-29",
            EN: ""
        }
    },
    {
        img: doc39,
        text: {
            RU: "Производственный план Морквашинской школы в 1928-29",
            EN: ""
        }
    },
    {
        img: doc40,
        text: {
            RU: "Производственный план Морквашинской школы в 1928-29",
            EN: ""
        }
    },
    {
        img: doc26,
        text: {
            RU: "",
            EN: ""
        }
    },
    {
        img: doc27,
        text: {
            RU: "",
            EN: ""
        }
    },
    {
        img: doc29,
        text: {
            RU: "",
            EN: ""
        }
    },
    {
        img: doc31,
        text: {
            RU: "",
            EN: ""
        }
    },
    {
        img: doc34,
        text: {
            RU: "01.06.1919 Создание Уезда и органов власти + прирез кучи населённых пунктов и их школ + речь-мотиватор Горанского на тему большей ответственности",
            EN: ""
        }
    },
    {
        img: doc35,
        text: {
            RU: "",
            EN: ""
        }
    },
    {
        img: doc36,
        text: {
            RU: "",
            EN: ""
        }
    },
    {
        img: doc38,
        text: {
            RU: "Протоколы ГорОНО",
            EN: ""
        }
    },
    {
        img: doc41,
        text: {
            RU: "Производственный план Хрящевской школы №2 в 1928-29",
            EN: ""
        }
    },
    {
        img: doc43,
        text: {
            RU: "Производственный план Хрящевской школы №2 в 1928-29",
            EN: ""
        }
    },
    {
        img: doc45,
        text: {
            RU: "Производственный план Хрящевской школы №2 в 1928-29",
            EN: ""
        }
    },
    {
        img: doc47,
        text: {
            RU: "Производственный план Хрящевской школы №2 в 1928-29",
            EN: ""
        }
    },
    {
        img: doc49,
        text: {
            RU: "Производственный план Хрящевской школы №2 в 1928-29",
            EN: ""
        }
    },
    {
        img: doc50,
        text: {
            RU: "Производственный план Хрящевской школы №2 в 1928-29",
            EN: ""
        }
    },
    {
        img: doc53,
        text: {
            RU: "Производственный план Хрящевской школы №2 в 1928-29",
            EN: ""
        }
    },
    {
        img: doc55,
        text: {
            RU: "Производственный план Хрящевской школы №2 в 1928-29",
            EN: ""
        }
    },
    {
        img: doc57,
        text: {
            RU: "Производственный план Хрящевской школы №2 в 1928-29",
            EN: ""
        }
    },
    {
        img: doc59,
        text: {
            RU: "Производственный план Хрящевской школы №2 в 1928-29",
            EN: ""
        }
    },
    {
        img: doc60,
        text: {
            RU: "Производственный план Хрящевской школы №2 в 1928-29",
            EN: ""
        }
    },
    {
        img: doc62,
        text: {
            RU: "Производственный план Хрящевской школы №2 в 1928-29",
            EN: ""
        }
    },
    {
        img: doc64,
        text: {
            RU: "Производственный план Хрящевской школы №2 в 1928-29",
            EN: ""
        }
    },
    {
        img: doc66,
        text: {
            RU: "Производственный план Хрящевской школы №2 в 1928-29",
            EN: ""
        }
    },
    {
        img: doc68,
        text: {
            RU: "Производственный план Хрящевской школы №2 в 1928-29",
            EN: ""
        }
    },
    {
        img: doc70,
        text: {
            RU: "Производственный план Хрящевской школы №2 в 1928-29",
            EN: ""
        }
    },
    {
        img: doc73,
        text: {
            RU: "Производственный план Хрящевской школы №2 в 1928-29",
            EN: ""
        }
    },
    {
        img: doc76,
        text: {
            RU: "Производственный план Хрящевской школы №2 в 1928-29",
            EN: ""
        }
    },
    {
        img: doc42,
        text: {
            RU: "Заседание школьных врачей г Ставрополя 19.06.1918 - ПЕРВЫЕ шаги к здоровью учеников",
            EN: ""
        }
    },
    {
        img: doc44,
        text: {
            RU: "Заседание медотдела - девочкам дать фельдшера-женщину",
            EN: ""
        }
    },
    {
        img: doc46,
        text: {
            RU: "Протокол - педагогов нет вводятся одногодичные курсы для ЕТШ",
            EN: ""
        }
    },
    {
        img: doc48,
        text: {
            RU: "",
            EN: ""
        }
    },
    {
        img: doc51,
        text: {
            RU: "Зав. Горанский - подготовка к ЕТШ",
            EN: ""
        }
    },
    {
        img: doc52,
        text: {
            RU: "Зав. Горанский - подготовка к ЕТШ",
            EN: ""
        }
    },
    {
        img: doc54,
        text: {
            RU: "",
            EN: ""
        }
    },
    {
        img: doc56,
        text: {
            RU: "Протокол 11.12.1918 - закрытие женской гимназии и открытие в ней учительской семинарии",
            EN: ""
        }
    },
    {
        img: doc58,
        text: {
            RU: "Запрет учить священослужителям + наём водовоза для школ + неотъём учебных помещений под другие цели",
            EN: ""
        }
    },
    {
        img: doc61,
        text: {
            RU: "Столовая для голодающих учеников",
            EN: ""
        }
    },
    {
        img: doc63,
        text: {
            RU: "",
            EN: ""
        }
    },
    {
        img: doc65,
        text: {
            RU: " Протокол 14.12.1918 - Создание родкомитетов",
            EN: ""
        }
    },
    {
        img: doc67,
        text: {
            RU: "Протокол 18.01.1919 - о воссоздании ремесленых училищ",
            EN: ""
        }
    },
    {
        img: doc69,
        text: {
            RU: "",
            EN: ""
        }
    },
    {
        img: doc71,
        text: {
            RU: "28.12.1918 - Открытие школы красноармейцев со 2 января",
            EN: ""
        }
    },
    {
        img: doc72,
        text: {
            RU: "Увольнение",
            EN: ""
        }
    },
    {
        img: doc74,
        text: {
            RU: "19.05.1919 - Реорганизация школьных советов",
            EN: ""
        }
    },
    {
        img: doc75,
        text: {
            RU: "Оставить работникам обра дачи",
            EN: ""
        }
    },
    {
        img: doc77,
        text: {
            RU: "23.05.1919 Выдача ученикам мыла",
            EN: ""
        }
    },
    {
        img: doc78,
        text: {
            RU: "",
            EN: ""
        }
    },
    {
        img: doc79,
        text: {
            RU: "Поездка Горанского в Мелекесс за одеждой и обувью + создание подотдела Снабжения",
            EN: ""
        }
    },
    {
        img: doc80,
        text: {
            RU: "26.05.1919 - назначен первый субботник",
            EN: ""
        }
    },
    {
        img: doc81,
        text: {
            RU: "Сбор лекарственных трав и цветов учениками",
            EN: ""
        }
    },
    {
        img: doc82,
        text: {
            RU: "",
            EN: ""
        }
    },
    {
        img: doc83,
        text: {
            RU: "Первая изба читальня - назначен заведующий",
            EN: ""
        }
    },
    {
        img: doc84,
        text: {
            RU: "Городской сад стал детской площадкой",
            EN: ""
        }
    },
    {
        img: doc85,
        text: {
            RU: "Перепись грамотных",
            EN: ""
        }
    },
    {
        img: doc86,
        text: {
            RU: "Возвращение Горанского",
            EN: ""
        }
    },
    {
        img: doc87,
        text: {
            RU: "План уроков с темой Наступление весны",
            EN: ""
        }
    },
    {
        img: doc88,
        text: {
            RU: "План уроков с темой Наступление весны",
            EN: ""
        }
    },
    {
        img: doc89,
        text: {
            RU: "План уроков с темой Наступление весны",
            EN: ""
        }
    },
    {
        img: doc90,
        text: {
            RU: "План уроков с темой Наступление весны",
            EN: ""
        }
    },
    {
        img: doc91,
        text: {
            RU: "План Ташёлской школы 1й ступени",
            EN: ""
        }
    },
    {
        img: doc92,
        text: {
            RU: "План Ташёлской школы 1й ступени",
            EN: ""
        }
    },
    {
        img: doc93,
        text: {
            RU: "План Ташёлской школы 1й ступени",
            EN: ""
        }
    },
    {
        img: doc94,
        text: {
            RU: "План Ташёлской школы 1й ступени",
            EN: ""
        }
    },
    {
        img: doc95,
        text: {
            RU: "План Ташёлской школы 1й ступени",
            EN: ""
        }
    },
    {
        img: doc96,
        text: {
            RU: "План Ташёлской школы 1й ступени",
            EN: ""
        }
    },
    {
        img: doc97,
        text: {
            RU: "План Ташёлской школы 1й ступени",
            EN: ""
        }
    },
    {
        img: doc98,
        text: {
            RU: "План Ташёлской школы 1й ступени",
            EN: ""
        }
    },
    {
        img: doc99,
        text: {
            RU: "План Ташёлской школы 1й ступени",
            EN: ""
        }
    },
    {
        img: doc100,
        text: {
            RU: "План Ташёлской школы 1й ступени",
            EN: ""
        }
    },

    {
        img: doc101,
        text: {
            RU: "",
            EN: ""
        }
    },
    {
        img: doc102,
        text: {
            RU: "",
            EN: ""
        }
    },
    {
        img: doc103,
        text: {
            RU: "",
            EN: ""
        }
    },
    {
        img: doc104,
        text: {
            RU: "",
            EN: ""
        }
    },
    {
        img: doc105,
        text: {
            RU: "",
            EN: ""
        }
    },
    {
        img: doc106,
        text: {
            RU: "",
            EN: ""
        }
    },
    {
        img: doc107,
        text: {
            RU: "",
            EN: ""
        }
    },
    {
        img: doc108,
        text: {
            RU: "",
            EN: ""
        }
    },
    {
        img: doc109,
        text: {
            RU: "",
            EN: ""
        }
    },
    {
        img: doc110,
        text: {
            RU: "",
            EN: ""
        }
    },
    {
        img: doc111,
        text: {
            RU: "",
            EN: ""
        }
    },
    {
        img: doc112,
        text: {
            RU: "",
            EN: ""
        }
    },
    {
        img: doc113,
        text: {
            RU: "",
            EN: ""
        }
    },
    {
        img: doc114,
        text: {
            RU: "",
            EN: ""
        }
    },
    {
        img: doc115,
        text: {
            RU: "",
            EN: ""
        }
    },
    {
        img: doc116,
        text: {
            RU: "",
            EN: ""
        }
    },
    {
        img: doc117,
        text: {
            RU: "",
            EN: ""
        }
    },
    {
        img: doc118,
        text: {
            RU: "",
            EN: ""
        }
    },
    {
        img: doc119,
        text: {
            RU: "",
            EN: ""
        }
    },
    {
        img: doc120,
        text: {
            RU: "",
            EN: ""
        }
    },
    {
        img: doc121,
        text: {
            RU: "",
            EN: ""
        }
    },
    {
        img: doc122,
        text: {
            RU: "",
            EN: ""
        }
    },
    {
        img: doc123,
        text: {
            RU: "",
            EN: ""
        }
    },

]