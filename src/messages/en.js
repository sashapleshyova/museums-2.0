import mainSlide from "./img/mainPage/111.jpg"

const en = {
    //Header
    header: {
        mainName: 'Togliatti Museum of education',
        links: [
            {
                name: 'Main',
                link: '/'
            },
            {
                name: 'History of education',
                link: '/History'
            },
            {
                name: 'Management in education',
                link: '/Management'
            },
            {
                name: 'Preschool education',
                link: '/Sort/kindergarten'
            },
            {
                name: 'School education',
                link: '/Sort/school'
            },
            {
                name: 'Secondary vocational education',
                link: '/Sort/averageSchool'
            },
            {
                name: 'Higher education',
                link: '/Sort/higherSchool'
            },
            {
                name: 'Additional education',
                link: '/AdditionalEducation'
            }
        ]
    },
    footer: {
        mainName: 'Togliatti Museum of education',
        support: 'With the support of the',
        TSU: 'Togliatti State University'
    },

    //Main
    main: {
        aboutProject: [
            {
                title: "",
                text: []
            },
            {
                title: "",
                text: []
            },
            {
                title: "",
                text: []
            },
            {
                title: "",
                text: []
            },
            {
                title: "",
                text: []
            },
            {
                title: "",
                text: []
            }
        ],
        mainSearch: {
            mainSearchTitle: 'Select the institution you are interested in',
            mainSearchExample: 'For example: school №13'
        },
        sections: {
            links: [
                {
                    name: 'History of education in the Stavropol-Togliatti',
                    link: '/History'
                },
                {
                    name: 'Education management',
                    link: '/Management'
                },
                {
                    name: 'The club of veterans of education',
                    link: '/VeteransClub'
                },
                {
                    name: 'Preschool education',
                    link: '/Sort/kindergarten'
                },
                {
                    name: 'School education',
                    link: '/Sort/school'
                },
                {
                    name: 'Additional education',
                    link: '/AdditionalEducation'
                },
                {
                    name: 'Higher education',
                    link: '/Sort/higherSchool'
                },
                {
                    name: 'Secondary vocational education',
                    link: '/Sort/averageSchool'
                }
            ]
        },
        main_slider: {
            slides: [
                {
                    photo: mainSlide,
                    text: ""
                }
            ]
        }
    },

    //History
    history: {
        title: "",
        content: [
            {
                title: "",
                text: []
            },
            {
                title: "",
                text: []
            },
            {
                title: "",
                text: []
            },
            {
                title: "",
                text: []
            },
        ]
    },

    //Management
    management: {
        title: "",
        content: [
            {
                title: "",
                text: [""]
            },
            {
                title: "",
                text: []
            },
            {
                title: "",
                text: []},
        ],
        management_in_togliatti: " ",
        links: [
            {
                name: "",
                link: "/LeadersOfEducation"
            },
            {
                name: "",
                link: "/table"
            },
            {
                name: "",
                link: "/table"
            },
            {
                name: "",
                link: "/table"
            },
            {
                name: "",
                link: "/documentation"
            },
        ],
        documents:""
    },
    filters: {
        sort: "Сортировать по районам",
        region: ["Автозаводской район", "Центральный район", "Комсомольский район"],
        button: "Применить",
        searchIn: {
            kindergarten: "Поиск по детским садам",
            school: "Поиск по школам",
            higherSchool: "Поиск по высшим учебным заведениям",
            averageSchool: "Поиск по среднее профессиональным учебным заведениям",
            artSchool: "Поиск по художественным школам",
            musicSchool: "Поиск по музыкальным школам",
            sportsSchool: "Поиск по спортивным школам",
            all: "Поиск по учебным заведениям"
        },
        sortIn: {
            kindergarten: "Сортировать по детским садам",
            school: "Сортировать по школам",
            higherSchool: "Сортировать по высшим учебным заведениям",
            averageSchool: "Сортировать по среднее профессиональным учебным заведениям",
            artSchool: "Сортировать по художественным школам",
            musicSchool: "Сортировать по музыкальным школам",
            sportsSchool: "Сортировать по спортивным школам",
            all: "Сортировать по учебным заведениям",
        },
        no: {
            kindergarten: "Детских садов нет",
            school: "Школ нет",
            higherSchool: "Высших учебных заведений нет",
            averageSchool: "Средне профессиональных учебных заведений нет",
            artSchool: "Художественных школ нет",
            musicSchool: "Музыкальных школ нет",
            sportsSchool: "спортивных школ нет",
            all: "Учебных заведений нет",
        },
    },
    //MapType
    map: {
        title: "Выберите интересующие вас учереждение",
        forExample: "Например: Найдите школу в которой вы училсь"
    },
    //OnAffinity
    onAffinity: {
        no: {
            kindergarten: "Детских садов поблизости нет",
            school: "Школ поблизости нет",
            higherSchool: "Высших учебных заведений поблизости нет",
            averageSchool: "Средне профессиональных учебных заведений поблизости нет",
            artSchool: "Художественных школ поблизости нет",
            musicSchool: "Музыкальных школ поблизости нет",
            sportsSchool: "спортивных школ поблизости нет",
        },
        yes: {
            kindergarten: "Детские сады поблизости",
            school: "Школы поблизости",
            higherSchool: "Высшие учебные заведения поблизости",
            averageSchool: "Средне профессиональные учебные заведения поблизости",
            artSchool: "Художественные школы поблизости",
            musicSchool: "Музыкальные школы поблизости",
            sportsSchool: "Спортивные школы поблизости",
        }

    },
    //OtherOverlay
    overlay: {
        more: "Читать далее...",
        level: {
            kindergarten: "Дошкольное образование:",
            school: "Школьное образование:",
            higherSchool: "Высшее образование:",
            averageSchool: "Среднее профессиональное образование:",
        }
    },
    //AdditionalEducation
    additionalEducation: {
        links: [
            {
                name: 'Художественные школы',
                link: '/Sort/artSchool'
            },
            {
                name: 'Музыкальные школы',
                link: '/Sort/musicSchool'
            },
            {
                name: 'Спортивные школы',
                link: '/Sort/sportsSchool'
            },
        ]
    },
    //Veterans
    veterans: {
        info: {
            more: "Читать далее",
            little: "Свернуть",
        },
        placeholder: "Например, Иванов Иван Иванович",
        title: "Найдите ветерана образования"
    },
    leadersOfEducation:""
};

export default en;