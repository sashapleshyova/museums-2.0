import school from './school';
import kindergarten from './kindergarten';
import sportSchool from './sportSchool';
import musicSchool from './musicSchool';
import artSchool from './artSchool';
import university from './university';
import college from './college';

const all = {
    school,
    kindergarten,
    sportSchool,
    musicSchool,
    artSchool,
    university,
    college
};
export default all;

