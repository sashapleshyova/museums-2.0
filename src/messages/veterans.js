import img1 from "./img/veterans/img1.JPG"
import img2 from "./img/veterans/img2.JPG"
import img3 from "./img/veterans/img3.JPG"
import img4 from "./img/veterans/img4.JPG"
import img5 from "./img/veterans/img5.JPG"
import img6 from "./img/veterans/img6.JPG"
import img7 from "./img/veterans/img7.JPG"
import img8 from "./img/veterans/img8.JPG"
import img9 from "./img/veterans/img9.JPG"
import img10 from "./img/veterans/img10.JPG"
import img11 from "./img/veterans/img11.JPG"
import img12 from "./img/veterans/img12.JPG"
import img13 from "./img/veterans/img13.JPG"
import img14 from "./img/veterans/img14.JPG"
import img15 from "./img/veterans/img15.JPG"
import img16 from "./img/veterans/img16.JPG"
import img17 from "./img/veterans/img17.JPG"
import img18 from "./img/veterans/img18.JPG"
import img19 from "./img/veterans/img19.JPG"
import img20 from "./img/veterans/img20.JPG"

import fik1 from "./img/veterans/fedoricheva_korneva (1).JPG"
import fik2 from "./img/veterans/fedoricheva_korneva (2).JPG"
import fik3 from "./img/veterans/fedoricheva_korneva (3).JPG"
import fik4 from "./img/veterans/fedoricheva_korneva (4).JPG"
import fik5 from "./img/veterans/fedoricheva_korneva (5).JPG"
import fik6 from "./img/veterans/fedoricheva_korneva (6).JPG"
import fik7 from "./img/veterans/fedoricheva_korneva (7).JPG"
import fik8 from "./img/veterans/fedoricheva_korneva (8).JPG"
import fik9 from "./img/veterans/fedoricheva_korneva (9).JPG"
import fik10 from "./img/veterans/fedoricheva_korneva (10).JPG"
import fik11 from "./img/veterans/fedoricheva_korneva (11).JPG"
import fik12 from "./img/veterans/fedoricheva_korneva (12).JPG"

const veterans = [
    {
        ID:0,
        name: {
            RU: "Антонова Галина Михайловна",
            EN: "",
        },
        miniInfo:{
            RU:"",
            EN:""
        },
        mainPhoto:img1 ,
        text: [
            // {
            //     title: {
            //         RU: "Биография",
            //         EN: ""
            //     },
            //     text: {
            //         RU: "Информация отсутствует",
            //         EN: "",
            //     }
            // },
            // {
            //     title: {
            //         RU: "Деятельность",
            //         EN: ""
            //     },
            //     text: {
            //         RU:"Информация отсутствует",
            //         EN: "",
            //     }
            // }
        ],
        media: {
            img: [img2,img3,img4,img5,img6,img7],
            video: ["https://youtu.be/plPDo4GTQwM"]
        }

    },
    {
        ID:1,
        name: {
            RU: "Перова Александра Васильевна",
            EN: "",
        },
        miniInfo:{
            RU:"",
            EN:""
        },
        mainPhoto: img8,
        text: [
            // {
            //     title: {
            //         RU: "Биография",
            //         EN: ""
            //     },
            //     text: {
            //         RU: "",
            //         EN: "",
            //     }
            // },
            // {
            //     title: {
            //         RU: "Деятельность",
            //         EN: ""
            //     },
            //     text: {
            //         RU:"",
            //         EN: "",
            //     }
            // }
        ],
        media: {
            img: [img9, img10, img11],
            video: ["https://youtu.be/f5oxdwNzZbY"]
        }

    },
    {
        ID:2,
        name: {
            RU: "Клавдия Александровна Борисова",
            EN: "",
        },
        miniInfo:{
            RU:"",
            EN:""
        },
        mainPhoto:img13,
        text: [
            // {
            //     title: {
            //         RU: "Биография",
            //         EN: ""
            //     },
            //     text: {
            //         RU: "",
            //         EN: "",
            //     }
            // },
            // {
            //     title: {
            //         RU: "Деятельность",
            //         EN: ""
            //     },
            //     text: {
            //         RU:"",
            //         EN: "",
            //     }
            // }
        ],
        media: {
            img: [img12,img14,img15, img16, img17,img18, img19, img20],
            video: ["https://youtu.be/rNYte7VDRck","https://youtu.be/L_xpvYY2Wfc"]
        }

    },
    {
        ID:3,
        name: {
            RU: "Регина Алексеевна Федоричева и Светлана Семеновна Корнеева",
            EN: "",
        },
        miniInfo:{
            RU:"",
            EN:""
        },
        mainPhoto:"" ,
        text: [
            // {
            //     title: {
            //         RU: "Биография",
            //         EN: ""
            //     },
            //     text: {
            //         RU: "",
            //         EN: "",
            //     }
            // },
            // {
            //     title: {
            //         RU: "Деятельность",
            //         EN: ""
            //     },
            //     text: {
            //         RU:"",
            //         EN: "",
            //     }
            // }
        ],
        media: {
            img: [fik1, fik2, fik3, fik4, fik5, fik6, fik7, fik8, fik9, fik10, fik11, fik12],
            video: ["https://youtu.be/rYx_naxjeLM"]
        }

    },
    // {
    //     ID:0,
    //     name: {
    //         RU: "",
    //         EN: "",
    //     },
    //     miniInfo:{
    //         RU:"",
    //         EN:""
    //     },
    //     mainPhoto: "",
    //     text: [
    //         {
    //             title: {
    //                 RU: "Биография",
    //                 EN: ""
    //             },
    //             text: {
    //                 RU: "",
    //                 EN: "",
    //             }
    //         },
    //         {
    //             title: {
    //                 RU: "Деятельность",
    //                 EN: ""
    //             },
    //             text: {
    //                 RU:"",
    //                 EN: "",
    //             }
    //         }
    //     ],
    //     media: {
    //         img: [],
    //         video: []
    //     }
    //
    // },

];
export default veterans;