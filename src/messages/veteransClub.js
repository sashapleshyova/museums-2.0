import mainSliderImg from "./img/veteransClub/still-life-851328_1280.jpg"
import veteransImage1 from "./img/veteransClub/022-ktfo9IXVqs4.jpg"
import veteransImage2 from "./img/veteransClub/021-A9y6VY9GtZU.jpg"
import veteransImage3 from "./img/veteransClub/036-GhoJW7X8pmQ.jpg"
import veteransImage4 from "./img/veteransClub/DSC_3301.JPG"
import veteransImage5 from "./img/veteransClub/DSC_3521.JPG"
import veteransImage6 from "./img/veteransClub/DSC_3540.JPG"
import veteransImage7 from "./img/veteransClub/DSC_3375.JPG"
import veteransImage8 from "./img/veteransClub/DSC_3546.JPG"
import veteransImage9 from "./img/veteransClub/P1070325.JPG"

export default {
    veteransSlider: [
        mainSliderImg,veteransImage1,veteransImage2,veteransImage3,veteransImage4,veteransImage5,veteransImage6,veteransImage7,veteransImage8,veteransImage9
    ],
    infoAboutVeteransClub: [
        {
            title: {
                RU: "",
                EN: ""
            },
            text: {
                RU: [
                    "В каждой школе есть педагоги, которых коллеги и ученики вспоминают долгие годы, даже после их ухода на пенсию. Они оставили после себя множество достижений и благодарных учеников, которые стали хорошими специалистами, а многие учителями. Именно поэтому было принято решение о создании Клуба ветеранов образования.",
                    "Клуб ветеранов образования состоит из педагогов, которые не равнодушны к истории образования и воспитания. Они помогают восстанавливать исторические материалы и о муниципальных образовательных учреждениях города Ставрополя-Тольятти, и о людях, которые учили и воспитывали детей в нашем городе. Многие из них до сих пор принимают активное участие в жизни образовательных учреждений, в творческих встречах и выставках. Делятся своим жизненным опытом с молодым поколением. Такое содружество приносит много доброго и содержательного в организацию образовательного процесса в современной школе.  Только развивая традиции прошлого и внедряя инновации сегодняшнего дня, мы сможем найти оптимальные варианты воспитания и обучения подрастающего поколения.",
                    "Мы надеемся, что бывшие выпускники образовательных учреждений нашего города  будут писать нам или звонить для того, чтобы продолжить написание истории  о педагогах, оставивших память в Ваших сердцах.\n"
                ],
                EN: []
            }
        },

    ]
}
