import {combineReducers} from 'redux';
import {routerReducer} from 'react-router-redux';
import Data from './redux/Data/reducers';
import States from './redux/States/reducers';
import StateVeteran from './redux/StateVeteran/reducers';
import Sort from './redux/Sort/reducers';

export default combineReducers({
    // routing: routerReducer,
    Data,
    States,
    StateVeteran,
    Sort
})