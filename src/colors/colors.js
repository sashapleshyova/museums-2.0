export const accentColor = 'rgb(8, 7, 58)';
export const borderInput = 'rgb(136, 136, 136)';
export const textInput = 'rgb(170, 170, 170)';
export const miniText = 'rgb(102,102,102)';
export const point='rgb(196,196,196)';