import {handleActions} from 'redux-actions';
import actions from './actions';

const initialState = {
    menuState: false,
    numberTable: 0,
    dropdownListState:false
};

export default handleActions({
    [actions.changeMenuState]: (state, action) => ({...state, menuState: !state.menuState}),
    [actions.changeNumberTable]: (state, action) => ({...state, numberTable: action.payload}),
    [actions.changeDropdownListState]: (state, action) => ({...state, dropdownListState: !state.dropdownListState}),
}, initialState)