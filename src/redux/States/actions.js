import { createActions } from 'redux-actions';

export default createActions({

  changeMenuState: () => {},

  changeNumberTable: (numberTable) => (numberTable),

  changeDropdownListState: () => {},
})