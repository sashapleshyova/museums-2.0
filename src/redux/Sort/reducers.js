import {handleActions} from 'redux-actions';
import actions from './actions';
import DataBase from './../../messages/allInstitution';

const initialState = {
  // DataBase:[],
  Districts:[0,1,2],
  Word:"",
  Type:"all",
  View:"filters",
};

export default handleActions({
  // [actions.setUp]: (state, action) => ({...state, DataBase:DataBase }),
  [actions.setDistricts]: (state, action) => ({...state, Districts:action.payload }),
  [actions.word]: (state, action) => ({...state, Word:action.payload }),
  [actions.type]: (state, action) => ({...state, Type:action.payload }),
  [actions.view]: (state, action) => ({...state, View:state.View==="filters"?"map":"filters" }),
  [actions.viewSet]: (state, action) => ({...state, View:"filters"}),
}, initialState)