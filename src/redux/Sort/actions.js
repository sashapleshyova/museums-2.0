import { createActions } from 'redux-actions';

export default createActions({
    setUp:()=>{},
    setDistricts:(districts)=>(districts),
    word:(word)=>(word),
    type:(type)=>(type),
    view:()=>{},
    viewSet:()=>{},
})