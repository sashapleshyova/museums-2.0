import {handleActions} from 'redux-actions';
import actions from './actions';
import ru from './../../messages/ru';
import en from './../../messages/en';

const initialState = {
  lang: 'RU',
  data: ru,
};

export default handleActions({
  [actions.lang]: (state, action) => ({...state, lang: state.lang === 'RU' ? 'EN' : 'RU'}),
  [actions.addLang]: (state, action) => ({...state, data: state.lang === 'RU' ? ru : en}),
}, initialState)