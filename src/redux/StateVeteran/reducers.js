import {handleActions} from 'redux-actions';
import actions from './actions';
import veterans from './../../messages/veterans';


const initialState = {
    open: false,
    veteranId:"0",
    wordVeteran:"",
    resultVeteransMas:[],
};

function sort(type,word) {
    let mas2 = [];

    for (let i=0; i<type.length; i++){
        if (type[i].name.RU.toUpperCase().indexOf(word.toUpperCase()) !== -1) {
            mas2.push(type[i]);
        }
    }
    console.log(mas2);
    return mas2;
}

export default handleActions({
    [actions.open]: (state,action)=>({...state,open:action.payload}),
    [actions.veteranId]: (state,action)=>({...state,veteranId:action.payload}),

    [actions.addVeteran]:(state,action)=>({...state,wordVeteran: action.payload}),
    [actions.searchVeteran]:(state, action)=>({...state, resultVeteransMas:sort(veterans, state.wordVeteran)})

}, initialState)