import React, {Component} from 'react';
import './style.scss';
import { connect } from 'react-redux';
import classNames from 'classnames';
import documentation from './../../messages/documentation'

class Documentation extends Component{
    constructor(props) {
        super(props);
        this.state = {
            choicePhoto:-1,
            page:0
        };
    }
    render(){
        const {Data} = this.props;
        return(
            <section className="Documentation">
                <h2>{ Data.data.management.documents}</h2>
                <div className="Documentation-Content container">
                    {
                        documentation.map((doc, index)=>{
                            return index>=this.state.page && index<this.state.page+20 ? <div className="Document" key={index}
                                        onClick={() => {
                                            this.setState({choicePhoto:index});
                                        }}>
                                <img src={(doc.img)}/>
                            </div> :null
                        })
                    }

                </div>
                <div className="PageNavigation">
                    {
                        [0,1,2,3,4,5,6].map((index)=>{
                            return <div className={classNames('ButtonPage', {'activeButton' : this.state.page/20===index})} key={index}
                                        onClick={() => {
                                            this.setState({page:index*20})
                                        }}>{index+1}</div>
                        })
                    }
                </div>
                {this.state.choicePhoto !== -1 ?
                    <div className="BackgroundBigPhoto"
                         onClick={() => {
                             this.setState({choicePhoto:-1})
                         }}>
                        <img className="BigPhoto" src={documentation[this.state.choicePhoto].img} />
                        <div className="Info">
                            {documentation[this.state.choicePhoto].text[this.props.Data.lang]}
                        </div>
                    </div>
                    : null}
            </section>
        )
    }
}

const mapStateToProps = (state) => ({
    Data: state.Data
});

const mapDispatchToProps = (dispatch) =>({});

export default connect(mapStateToProps, mapDispatchToProps)(Documentation)