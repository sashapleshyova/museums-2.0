import React, {Component} from 'react';
import './style.scss';
import {connect} from 'react-redux';
import {Link} from "react-router-dom";
import classNames from 'classnames';
import veterans from "../../../messages/veterans";

class Info extends Component {

    render() {
        const {Data} = this.props;
        return (
            <section className="Biography">
                <div className="Biography-Content container">
                    <div className="Left">
                        {veterans[this.props.StateVeteran.veteranId].mainPhoto==="" ? <div className="NoPhoto"/> : <img src={veterans[this.props.StateVeteran.veteranId].mainPhoto} className="Photo"/>}

                        <h3 className="Name">{veterans[this.props.StateVeteran.veteranId].name[this.props.Data.lang]}</h3>
                    </div>
                    <div className="Right">
                        {veterans[this.props.StateVeteran.veteranId].text.map((article, index)=>{
                            return <div className="Article" key={index}>
                                <h4>{article.title[this.props.Data.lang]}</h4>
                                <p>{article.text[this.props.Data.lang]}</p>
                            </div>
                        })}
                    </div>
                </div>
            </section>
        )
    }
}

const mapStateToProps = (state) => ({
    Data: state.Data,
    StateVeteran: state.StateVeteran
});
const mapDispatchToProps = (dispatch) => ({
});
export default connect(mapStateToProps, mapDispatchToProps)(Info);
