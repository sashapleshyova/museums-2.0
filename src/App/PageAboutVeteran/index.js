import React, {Component} from 'react';
import {connect} from 'react-redux';
import Info from './Biography';
import Media from './Media';
import actions from "../../redux/StateVeteran/actions";
import veterans from "../../messages/veterans";
import ActionSort from "../../redux/Sort/actions";


class PageAboutVeteran extends Component {
    componentDidMount() {
        window.scrollTo(0, 0);
        this.props.veteranId(this.props.match.params.vet);
        this.props.setDistricts([0,1,2]);
        this.props.setUp();
        this.props.word("");
        this.props.viewSet();
        this.props.type("all");
    }

    render() {
        return (
            this.props.match.params.vet<veterans.length ?
            <div className="PageAboutVeteran">
                <Info/>
                <Media/>
            </div> : null
        );
    }
}

const mapStateToProps = (state) => ({
    StateVeteran: state.StateVeteran
});
const mapDispatchToProps = (dispatch) => ({
    veteranId: (number) => dispatch(actions.veteranId(number)),
    setDistricts: (district) => dispatch(ActionSort.setDistricts(district)),
    setUp: () => dispatch(ActionSort.setUp()),
    word: (word) => dispatch(ActionSort.word(word)),
    viewSet: () => dispatch(ActionSort.viewSet()),
    type:(type)=>dispatch(ActionSort.type(type)),
});
export default connect(mapStateToProps, mapDispatchToProps)(PageAboutVeteran);

