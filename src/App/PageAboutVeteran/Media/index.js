import React, {Component} from 'react';
import './style.scss';
import {connect} from 'react-redux';
import {Link} from "react-router-dom";
import classNames from 'classnames';
import Slider from "react-slick/lib";
import veterans from "../../../messages/veterans";
import ReactPlayer from "react-player";


let veteransMedia = {
    dots: false,
    dotsClass: 'autoplay',
    infinite: false,
    speed: 500,
    initialSlide: 0,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: false,
    autoplaySpeed: 3000,
};
class Media extends Component {
    constructor(props) {
        super(props);
        this.state = {
            num: "0",
            type: "img"
        }
    }

    render() {
        const {Data} = this.props;

        return (
            <section className="Media">
                <div className="Media-Content container">
                    <img src={veterans[this.props.StateVeteran.veteranId].media.img[this.state.num]}
                         className="BigView"
                         style={{
                             display: this.state.type === "img" ? "flex" : "none"
                         }}/>

                    <div className='player-wrapper'
                         style={{
                             display: this.state.type === "video" ? "flex" : "none"
                         }}>
                        <ReactPlayer
                            className='react-player'
                            url={veterans[this.props.StateVeteran.veteranId].media.video[this.state.num]}
                            width='1000px'
                            height='600px'
                            controls='true'
                        />



                    </div>

                    <Slider {...veteransMedia}>
                        {veterans[this.props.StateVeteran.veteranId].media.img.map((img, indexImg) => {
                            return <div className="SlideContent" key={indexImg}>
                                <img src={img} className="MediaContent"
                                     onClick={() => {
                                         this.setState({num: indexImg});
                                         this.setState({type: "img"});
                                     }}/>
                            </div>
                        })}
                        {veterans[this.props.StateVeteran.veteranId].media.video.map((video, indexVideo) => {
                            return <div className="SlideContent" key={indexVideo}>
                                <div className="Video"
                                     style={{
                                         background: this.state.num === indexVideo ? '#dfdefa' : null
                                     }}
                                     onClick={() => {
                                         this.setState({num: indexVideo});
                                         this.setState({type: "video"});
                                     }}>
                                    <div className="ImageVideo"/>
                                    Смотреть видео
                                </div>

                            </div>
                        })}

                    </Slider>
                </div>
            </section>
        )
    }
}

const mapStateToProps = (state) => ({
    Data: state.Data,
    StateVeteran: state.StateVeteran
});
const mapDispatchToProps = (dispatch) => ({});
export default connect(mapStateToProps, mapDispatchToProps)(Media);
