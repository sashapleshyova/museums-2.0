import React, {Component} from 'react';
import './style.scss';
import {connect} from 'react-redux';
import classNames from 'classnames';
import {Link} from "react-router-dom";
import ActionSort from './../../../redux/Sort/actions';
import DataBase from './../../../messages/allInstitution';

let masRegionLocal = [];

class Filters extends Component {
    constructor(props) {
        super(props);
        for (let j = 0; j < 3; j++) {
            this.state = {...this.state, ['pointRegion' + j]: false}
        }

    }


    componentDidMount() {
        masRegionLocal = [];
    }

    render() {
        const {Data} = this.props;
        const {Sort} = this.props;

        // let strSearch = "В запросе ничего не написано";

        function makeWord() {
            let text = document.getElementsByTagName("input")[0];
            return text.value;
        }

        function per(mass) {
            let res = [];
            for (let i = 0; i < mass.length; i++) {
                res.push(mass[i]);
            }
            return res;
        }


        function box(mas, numberPoint) {
            let check = 0;
            let number;

            for (let i = 0; i < mas.length; i++) {
                if (mas[i] === numberPoint) {
                    check = 1;
                    number = i;
                }
            }
            if (check === 0) {
                mas.push(numberPoint);
            }
            else {
                mas.splice(number, 1);
            }
            return mas;
        }

        const nameMas = ["school", "kindergarten", "sportSchool", "musicSchool", "artSchool", "university", "college"];
        return (

            <section className="Filters">
                <div className="Filters-Content container">
                    <h4></h4>
                    <div className="Basic">
                        <div className="FiltersBlock">
                            <h5>{Data.data.filters.sortIn[this.props.Sort.Type]}</h5>
                            <div className="SearchField">
                                <input placeholder={Data.data.main.mainSearch.mainSearchExample}/>
                                {/*<div className="MainSearchIcon" onClick={() => {*/}
                                {/*this.props.addWord(makeWord());*/}
                                {/*this.props.searching();*/}
                                {/*}}>*/}
                                {/*<div className="MainSearchImage"/>*/}
                                {/*</div>*/}
                            </div>
                            <h5>{Data.data.filters.sort}</h5>
                            {
                                [0, 1, 2].map((index) => {

                                    return <div className="Option" key={index}
                                                onClick={() => {
                                                    this.setState({['pointRegion' + index]: !this.state['pointRegion' + index]});
                                                    console.log(box(masRegionLocal, index));
                                                }}>
                                        <div className="Point">
                                            <div
                                                className={classNames('Round', {'active': this.state['pointRegion' + index] === true})}/>
                                        </div>
                                        {Data.data.filters.region[index]}
                                    </div>
                                })
                            }
                            {/*<h5>{Data.data.filters.sortIn[this.props.TypeInstitution.typeInstitution]}</h5>*/}
                            {/*{*/}
                            {/*kindergarten.map((point, index) => {*/}
                            {/*return <div className="OptionSchool" key={index}*/}
                            {/*onClick={() => {*/}
                            {/*this.setState({['pointInstitute' + index]: !this.state['pointInstitute' + index]});*/}
                            {/*console.log(box(masInstitutionLocal, index))*/}
                            {/*}}>*/}
                            {/*<div className="Point">*/}
                            {/*<div*/}
                            {/*className={classNames('Round', {'active': this.state['pointInstitute' + index] === true})}/>*/}
                            {/*</div>*/}
                            {/*{point.mainTitle[this.props.Data.lang]}*/}
                            {/*</div>*/}
                            {/*})*/}
                            {/*}*/}
                            <div className="ButtonBlock">
                                <div className="Apply"
                                     onClick={() => {
                                         this.props.setDistricts(per(masRegionLocal));
                                         this.props.word(makeWord());
                                     }}>{Data.data.filters.button}</div>
                                {/*{console.log(this.props.TypeInstitution.type)}*/}
                                <div className="Map"
                                     onClick={() => {
                                         this.props.view();
                                     }}>
                                    <div className="Image"/>
                                </div>
                            </div>
                        </div>


                        {
                                DataBase.map((card, cardIndex) => {

                                    return (card.type === this.props.Sort.Type || this.props.Sort.Type === "all") && card.mainTitle.RU.toUpperCase().indexOf(this.props.Sort.Word.toUpperCase()) !== -1 && this.props.Sort.Districts.indexOf(card.area) !== -1 ?
                                        <Link to={"/Inst/" + card.ID}
                                            className="Card" key={cardIndex}>
                                            { card.mainImg !=="" ?  <img className="Photo" src={card.mainImg}/> : <div className="NoPhoto"/>}

                                            <div className="NameSchool"> {card.mainTitle[this.props.Data.lang]}</div>
                                        </Link> : null

                                })
                        }


                    </div>


                </div>
            </section>
        )
    }
}

const mapStateToProps = (state) => ({
    Data: state.Data,
    Sort: state.Sort,
});

const mapDispatchToProps = (dispatch) => ({
    setDistricts: (district) => dispatch(ActionSort.setDistricts(district)),
    setUp: () => dispatch(ActionSort.setUp()),
    word: (word) => dispatch(ActionSort.word(word)),
    view: () => dispatch(ActionSort.view()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Filters);
