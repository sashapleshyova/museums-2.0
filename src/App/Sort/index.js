import React, {Component} from 'react';
import {connect} from 'react-redux';
import Filters from './Filters';
import ActionSort from "../../redux/Sort/actions";
import MapType from './MapType';
import RandomOverlay from './RandomOverlay';


class Sort extends Component {
    componentDidMount() {
        window.scrollTo(0,0);
        this.props.setUp();
        this.props.type(this.props.match.params.id);
    }

    render() {

        return (
            <div className="Sort">
                <RandomOverlay/>
                {this.props.Sort.View==="filters" ? <Filters/> : <MapType/>}
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    Sort:state.Sort,
});

const mapDispatchToProps = (dispatch) => ({
    type:(type)=>dispatch(ActionSort.type(type)),
    setUp: () => dispatch(ActionSort.setUp()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Sort);



