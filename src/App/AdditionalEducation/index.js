import React, {Component} from 'react';
import {connect} from 'react-redux';
import OtherOverlay from './OtherOverlay';
import TypeOfSchool from './TypeOfSchool';
import ActionSort from "../../redux/Sort/actions";
import SeeAtMap from './SeeAtMap';



class AdditionalEducation extends Component {
    componentDidMount() {
        window.scrollTo(0,0);
        this.props.setDistricts([0,1,2]);
        this.props.setUp();
        this.props.word("");
        this.props.viewSet();
        this.props.type("all");
        // this.props.type(this.props.match.params.id);
    }
    render() {

        return (
            <div className="AdditionalEducation">
                <OtherOverlay/>
                {this.props.Sort.View==="filters" ?
                    <TypeOfSchool/>
                    :
                    <SeeAtMap/>
                }
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    Data: state.Data,
    Sort: state.Sort,
});

const mapDispatchToProps = (dispatch) => ({
    setDistricts: (district) => dispatch(ActionSort.setDistricts(district)),
    setUp: () => dispatch(ActionSort.setUp()),
    word: (word) => dispatch(ActionSort.word(word)),
    viewSet: () => dispatch(ActionSort.viewSet()),
    type:(type)=>dispatch(ActionSort.type(type)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AdditionalEducation);


