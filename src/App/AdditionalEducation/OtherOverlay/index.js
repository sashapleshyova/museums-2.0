import React, {Component} from 'react';
import './style.scss';
import {connect} from 'react-redux';
import classNames from 'classnames';
import {Link} from "react-router-dom";
import DataBase from "./../../../messages/allInstitution"


class OtherOverlay extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    render() {
        let randomId=-1;
        const {Data} = this.props;
       DataBase.length !==0 ?
            randomId = Math.floor(Math.random() * ( DataBase.length - 1)) : -1;
        return (
           DataBase[randomId]!==undefined ?
            <section className="OtherOverlay">
                <div className="Image">
                    <img
                        src={DataBase[randomId].mainImg}/>
                    <div className="BackgroundFilter"/>
                </div>

                <div className="OtherOverlay-Content container">
                    <h6>{Data.data.overlay.level[DataBase[randomId].type]}</h6>
                    <h4>{DataBase[randomId].mainTitle[this.props.Data.lang]}</h4>
                    <p>{DataBase[randomId].text[0].text[this.props.Data.lang]}</p>
                    <Link to={"/Inst/" + DataBase[randomId].ID} className="More">
                        {Data.data.overlay.more}
                    </Link>
                </div>
            </section>: null
        )
    }
}

const mapStateToProps = (state) => ({
    Data: state.Data,
    Sort: state.Sort
});

const mapDispatchToProps = (dispatch) => ({

});

export default connect(mapStateToProps, mapDispatchToProps)(OtherOverlay);
