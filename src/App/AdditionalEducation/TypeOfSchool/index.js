import React, {Component} from 'react';
import './style.scss';
import {connect} from 'react-redux';
import classNames from 'classnames';
import {Link} from "react-router-dom";
import ActionSort from "../../../redux/Sort/actions";
import dataBase from "./../../../messages/allInstitution"


class TypeOfSchool extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {

        const {Data} = this.props;
        // const randomId = Math.random() * (this.props.TypeInstitution.type.length - 1);
        return (
            <section className="TypeOfSchool">
                <div className="TypeOfSchool-Content container">
                    <div className="HorizontalMenu">
                        <h4>{Data.data.map.title}</h4>
                        {/*<div className="SearchField">*/}
                        {/*<input placeholder={Data.data.map.forExample}/>*/}
                        {/*<div className="MainSearchIcon" onClick={() => {*/}
                        {/*}}>*/}
                        {/*<div className="MainSearchImage"/>*/}
                        {/*</div>*/}
                        {/*</div>*/}
                        <div className="FiltersList"
                             onClick={() => {
                                 this.props.view();
                             }}>
                            <div className="Icon"/>
                        </div>
                    </div>

                    <div className="Type">
                        {
                            Data.data.additionalEducation.links.map((link, index) => {
                                return <Link to={link.link} className="SectionCard" key={index}>
                                    <div className={classNames('SectionPhoto' + index)}/>
                                    <div className="NameCategory">{link.name}</div>
                                </Link>
                            })
                        }
                        {
                            dataBase.map((ins, index)=>{
                                return ins.type==="additionalEducation" ?
                                <Link to={"/Inst/"+ins.ID} className="SectionCard" key={index}>
                                    <img src={ins.mainImg}/>
                                    <div className="NameCategory">{ins.mainTitle[this.props.Data.lang]}</div>
                                </Link> : null
                            })
                        }
                    </div>

                </div>
            </section>
        )
    }
}

const mapStateToProps = (state) => ({
    Data: state.Data,
    Sort: state.Sort
});

const mapDispatchToProps = (dispatch) => ({
    view: () => dispatch(ActionSort.view()),
});

export default connect(mapStateToProps, mapDispatchToProps)(TypeOfSchool);
