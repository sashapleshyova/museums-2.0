import React, {Component} from 'react';
import './style.scss';
import {connect} from 'react-redux';
import {Link} from "react-router-dom";
import classNames from 'classnames';
import {YMaps, Map, GeoObject, Rectangle, Placemark} from 'react-yandex-maps';
import icon from "./img/1.png"
import ActionSort from "../../../redux/Sort/actions";

class SeeAtMap extends Component {

    render() {
        const {Data} = this.props;
        const mapState = {center: [53.523231, 49.394206], zoom: 12, controls: []};
        return (
            <section className="SeeAtMap">
                <div className="SeeAtMap-Content container">
                    <div className="HorizontalMenu">
                        <h4>{Data.data.map.title}</h4>
                        <div className="SearchField">
                            <input placeholder={Data.data.map.forExample}/>
                            <div className="MainSearchIcon" onClick={() => {
                            }}>
                                <div className="MainSearchImage"/>
                            </div>
                        </div>
                        <div className="FiltersList"
                             onClick={() => {
                                 this.props.view("filters");
                             }}>
                            <div className="Icon"/>
                        </div>
                    </div>
                    <YMaps>
                        <div id="map-basics">
                            <Map state={mapState} width={1240} height={550}>
                                <Rectangle
                                    geometry={{
                                        coordinates: [[0, 0], [100, 100]],
                                    }}
                                    options={{
                                        fillColor: '#000',
                                        fillOpacity: 0.4,
                                    }}
                                />
                                {this.props.Sort.DataBase.map((place, index) => {
                                    return place.type==="artSchool" || place.type==="musicSchool" || place.type==="sportsSchool" ?
                                    <Placemark key={index}
                                                      geometry={{
                                                          coordinates: place.coordinates
                                                      }}
                                                      properties={{
                                                          hintContent: place.mainTitle[this.props.Data.lang],
                                                          balloonContent: place.mainTitle[this.props.Data.lang]+place.address[this.props.Data.lang],
                                                          image:place.mainImg,
                                                      }}
                                                      options={{
                                                          iconLayout: 'default#image',
                                                          iconImageHref:icon,
                                                          iconImageSize: [32, 32],
                                                      }}
                                    /> : null
                                })}
                            </Map>
                        </div>
                    </YMaps>
                </div>
            </section>
        )
    }
}

const mapStateToProps = (state) => ({
    Data: state.Data,
    Sort: state.Sort
});
const mapDispatchToProps = (dispatch) => ({
    view: () => dispatch(ActionSort.view()),
});
export default connect(mapStateToProps, mapDispatchToProps)(SeeAtMap);
