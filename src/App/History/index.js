import React, {Component} from 'react';
import './style.scss';
import { connect } from 'react-redux';
import ActionSort from "../../redux/Sort/actions";
import ReactPlayer from "react-player";

class History extends Component{
    componentDidMount() {
        window.scrollTo(0,0);
        this.props.setDistricts([0,1,2]);
        this.props.setUp();
        this.props.word("");
        this.props.viewSet();
        this.props.type("all");
    }
  render(){

    const {Data} = this.props;

    return(
      <div className="History">
        <section className="History-Content container">
          <h2 className="History-Content-Title">{Data.data.history.title}</h2>

            {
                Data.data.history.content.map((ras, index)=>{
                    return <div className="InfoBlock" key={index}>
                        <h3>{ras.title}</h3>
                            {
                                ras.text.map((abz, indexAbz)=>{
                                    return  <p key={indexAbz}>{abz}</p>
                                })
                            }

                    </div>
                })
            }
            <div className='player-wrapper'>
                <ReactPlayer
                    className='react-player'
                    url="https://youtu.be/kzTMR-Kb0rI"
                    width='1000px'
                    height='600px'
                    controls='true'
                />



            </div>
        </section>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  Data: state.Data
});

const mapDispatchToProps = (dispatch) => ({
    setDistricts: (district) => dispatch(ActionSort.setDistricts(district)),
    setUp: () => dispatch(ActionSort.setUp()),
    word: (word) => dispatch(ActionSort.word(word)),
    viewSet: () => dispatch(ActionSort.viewSet()),
    type:(type)=>dispatch(ActionSort.type(type)),
});

export default connect(mapStateToProps, mapDispatchToProps)(History);