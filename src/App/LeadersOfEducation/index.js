import React, {Component} from 'react';
import './style.scss';
import {connect} from 'react-redux';
import actions from "../../redux/StateVeteran/actions";
import ActionSort from "../../redux/Sort/actions";
import Slider from "react-slick/lib";
import classNames from "classnames";
import LeadersTable from "./../../messages/tableLeaders"

class LeadersOfEducation extends Component {

    constructor(props) {
        super(props);
        this.state = {
            activeState: -1,
            number: 0,
            stateFlyWindow: false,
        };
    }


    componentDidMount() {
        window.scrollTo(0, 0);
    }

    render() {
        let SettingTwo = {
            dots: true,
            dotsClass: 'responsive',
            infinite: false,
            speed: 500,
            rows: 2,
            initialSlide: 0,
            slidesToShow: 4,
            slidesToScroll: 4,
            autoplay: false,
        };
        const {Data} = this.props;

        return (
            <div className="LeadersOfEducation">
                <h2>{Data.data.leadersOfEducation}</h2>
                <section className="LeadersOfEducation-Content container">
                    {
                        LeadersTable.map((cardOne, index) => {
                            return <div className="CardSliderTwo" key={index}
                                        onMouseOver={() => {
                                            this.setState({activeState: index})
                                        }}
                                        onMouseLeave={() => {
                                            this.setState({activeState: -1});
                                        }}
                                        onClick={() => {
                                            this.setState({stateFlyWindow: true});
                                            this.setState({number: index});
                                        }}>
                                {cardOne.img !== "" ? <img src={(cardOne.img)}/> :
                                    <div className="NoPhoto"/>}
                                <h5>{(cardOne.Name[this.props.Data.lang])}</h5>
                                <div className="MiniInfo">{(cardOne.type[this.props.Data.lang])}</div>
                            </div>
                        })
                    }
                    <div
                        className={classNames('FlyWindow', {'activeFlyWindow': this.state.stateFlyWindow === true && LeadersTable[this.state.number].hoverTxt[this.props.Data.lang].length !== 0})}
                        onClick={() => {
                            this.setState({stateFlyWindow: false})
                        }}>
                        <div className="WhiteWindow">
                            <div className="WhiteWindow-Left">
                                {/*<img className="Photo" src={inst.slider_2.slides[this.state.number].img}/>*/}
                                {LeadersTable[this.state.number].img !== "" ?
                                    <img className="Photo" src={LeadersTable[this.state.number].img}/> :
                                    <div className="NoPhoto"/>}
                                <h4 className="Name">
                                    {LeadersTable[this.state.number].Name[this.props.Data.lang]}
                                </h4>
                                <div className="MiniInfo">
                                    {LeadersTable[this.state.number].type[this.props.Data.lang]}
                                </div>
                            </div>
                            <div className="WhiteWindow-Right">
                                {LeadersTable[this.state.number].hoverTxt[this.props.Data.lang].map((abz, index) => {
                                    return <p className="Info" key={index}>
                                        {abz}
                                    </p>
                                })}
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    Data: state.Data
});


const mapDispatchToProps = (dispatch) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(LeadersOfEducation);