import React, {Component} from 'react';
import './style.scss';
import {connect} from 'react-redux';
import Overlay from './Overlay';
import InfoText from './InfoText';
import Person from './Person';
import TeachersOrEducators from './TeachersOrEducators';
import OnAffinity from './OnAffinity'
import AwardedToStaff from './AwardedToStaff'
import Rewards from './Rewards'
// import Filters from "../Sort/Filters";
// import MapType from "../Sort/MapType";
import OnAffinityMap from "./OnAffinityMap"
import SliderFore from "./SliderFore"
import SliderSeven from "./SliderSeven"
import SliderThree from "./SliderThree"
import ActionSort from "../../redux/Sort/actions";
import DataBase from './../../messages/allInstitution';

class InstitutionTemplates extends Component {
    componentDidMount() {
        window.scrollTo(0, 0);
        this.props.setDistricts([0, 1, 2]);
        this.props.setUp();
        this.props.word("");
        this.props.viewSet();
        this.props.type("all");
    }

    render() {

        return (
           DataBase.map((inst, instIndex) => {
                return Number(this.props.match.params.id) === inst.ID ? <div className="Main" key={instIndex}>
                    {console.log("*"+inst.ID+"*")}
                        <Overlay inst={inst}/>
                        <InfoText inst={inst}/>
                        <Person inst={inst}/>
                        <TeachersOrEducators inst={inst}/>
                        <AwardedToStaff inst={inst}/>
                        <SliderFore inst={inst}/>
                        <SliderSeven inst={inst}/>
                        <SliderThree inst={inst}/>
                        <Rewards inst={inst}/>
                        {this.props.Sort.View === "filters" ?
                        <OnAffinity inst={inst}/>
                            :
                        <OnAffinityMap inst={inst}/>}

                    </div>
                    : null
            })

        );
    }
}

const mapStateToProps = (state) => ({
    Sort: state.Sort,
});

const mapDispatchToProps = (dispatch) => ({
    setDistricts: (district) => dispatch(ActionSort.setDistricts(district)),
    setUp: () => dispatch(ActionSort.setUp()),
    word: (word) => dispatch(ActionSort.word(word)),
    viewSet: () => dispatch(ActionSort.viewSet()),
    type: (type) => dispatch(ActionSort.type(type)),
});

export default connect(mapStateToProps, mapDispatchToProps)(InstitutionTemplates);