import React, {Component} from 'react';
import './style.scss';
import {connect} from 'react-redux';
import {Link} from "react-router-dom";
import ActionSort from "../../../redux/Sort/actions";
import DataBase from './../../../messages/allInstitution';

class OnAffinity extends Component {


    render() {
        const {Data} = this.props;
        const {inst} = this.props;
        return (
            <section className="OnAffinity">
                <div className="OnAffinity-Content container">
                    <div className="Line">
                        <h4>{Data.data.onAffinity.yes[inst.type]}</h4>
                        <div className="Map"
                             onClick={() => {
                                 this.props.view();
                             }}
                             style={{
                                 display: inst.schoolNear.length === 0 ? 'none' : 'flex'
                             }}>
                            <div className="Image"/>
                        </div>
                    </div>
                    {inst.schoolNear.length === 0 ? <div>{Data.data.onAffinity.no[inst.type]}</div> :
                        <div className="Box">
                            {inst.schoolNear.map((card, index) => {
                                return <Link className="Card" key={index}
                                             to={"/Inst/" + DataBase[card].ID}
                                             onClick={() => {
                                                 window.scrollTo(0, 0);
                                             }}>
                                    {DataBase[card].mainImg !=="" ?  <img className="Photo" src={DataBase[card].mainImg}/> : <div className="NoPhoto"/>}

                                    <div className="NameSchool"> {DataBase[card].mainTitle[this.props.Data.lang]}</div>
                                </Link>
                            })
                            }
                        </div>
                    }


                </div>
            </section>
        )
    }
}

const mapStateToProps = (state) => ({
    Data: state.Data,
    Sort: state.Sort
});
const mapDispatchToProps = (dispatch) => ({
    view: () => dispatch(ActionSort.view()),
});
export default connect(mapStateToProps, mapDispatchToProps)(OnAffinity);
