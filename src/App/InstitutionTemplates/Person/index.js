import React, {Component} from 'react';
import './style.scss';
import {connect} from 'react-redux';
import Slider from 'react-slick';
import classNames from 'classnames';

let Setting = {
    dots: true,
    dotsClass: 'responsive',
    infinite: false,
    speed: 500,
    initialSlide: 0,
    slidesToShow: 3,
    slidesToScroll: 3,
    autoplay: false,
};

class Person extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activeState: -1,
            number: 0,
            stateFlyWindow:false,
        };
    }

    render() {
        const {Data} = this.props;
        const {inst} = this.props;
        return (
            <section className="Person">
                <div className="Person-Content container">


                    {inst.slider_1 !== undefined ? <div>
                        <h4>{(inst.slider_1.title[this.props.Data.lang])}</h4>
                        <Slider {...Setting}>
                            {
                                inst.slider_1.slides.map((cardOne, index) => {
                                    return <div className="CardSliderOne" key={index}
                                                onMouseOver={() => {
                                                    this.setState({activeState: index})
                                                }}
                                                onMouseLeave={() => {
                                                        this.setState({activeState: -1});

                                                }}
                                                onClick={() => {
                                                    this.setState({stateFlyWindow: true});
                                                    this.setState({number: index});
                                                }}>
                                        {cardOne.img !=="" ?  <img src={(cardOne.img)}/> : <div className="NoPhoto"/>}
                                        <div className="RightInfo">
                                            <h5>{(cardOne.Name[this.props.Data.lang])}</h5>
                                            <div className="MiniInfo">{(cardOne.type[this.props.Data.lang])}</div>
                                        </div>
                                        {/*<div className={classNames('HoverWindow', {'activeWindow': this.state.activeState === index && cardOne.hoverTxt[this.props.Data.lang]!==""})}>*/}
                                            {/*<div className="HoverWindow-Content"> {(cardOne.hoverTxt[this.props.Data.lang])}</div>*/}
                                            {/*<div className="Triangle"/>*/}
                                            {/*</div>*/}
                                    </div>
                                })
                            }
                        </Slider>
                        <div className={classNames('FlyWindow', {'activeFlyWindow':this.state.stateFlyWindow===true && inst.slider_1.slides[this.state.number].hoverTxt[this.props.Data.lang].length!==0})}
                             onClick={() => {
                                 this.setState({stateFlyWindow: false})
                             }}>
                            <div className="WhiteWindow">
                                <div className="WhiteWindow-Left">
                                    {/*<img className="Photo" src={inst.slider_1.slides[this.state.number].img}/>*/}
                                    {inst.slider_1.slides[this.state.number].img !=="" ?  <img className="Photo" src={inst.slider_1.slides[this.state.number].img}/> : <div className="NoPhoto"/>}
                                    <h4 className="Name">
                                        {inst.slider_1.slides[this.state.number].Name[this.props.Data.lang]}
                                    </h4>
                                    <div className="MiniInfo">
                                        {inst.slider_1.slides[this.state.number].type[this.props.Data.lang]}
                                    </div>
                                </div>
                                <div className="WhiteWindow-Right">
                                    {inst.slider_1.slides[this.state.number].hoverTxt[this.props.Data.lang].map((abz,index)=>{
                                        return <p className="Info" key={index}>
                                            {abz}
                                        </p>
                                    })}

                                </div>
                            </div>
                        </div>
                    </div> : null}

                </div>

            </section>
        )
    }
}

const mapStateToProps = (state) => ({
    Data: state.Data
});

export default connect(mapStateToProps)(Person);