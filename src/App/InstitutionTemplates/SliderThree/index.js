import React, {Component} from 'react';
import './style.scss';
import {connect} from 'react-redux';
import Slider from 'react-slick';
import classNames from "classnames";

let Setting = {
    dots: true,
    dotsClass: 'responsive',
    infinite: false,
    speed: 500,
    initialSlide: 0,
    slidesToShow: 4,
    slidesToScroll: 4,
    autoplay: false,
};
let SettingTwo = {
    dots: true,
    dotsClass: 'responsive',
    infinite: false,
    speed: 500,
    rows: 2,
    initialSlide: 0,
    slidesToShow: 4,
    slidesToScroll: 4,
    autoplay: false,
};

class SliderThree extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activeState: -1,
            number: 0,
            stateFlyWindow:false,
        };
    }

    render() {
        const {Data} = this.props;
        const {inst} = this.props;
        return (
            <section className="SliderThree">
                <div className="SliderThree-Content container">


                    {inst.slider_3 !== undefined ? <div>
                        <h4>{(inst.slider_3.title[this.props.Data.lang])}</h4>

                        {inst.slider_3.slides.length <= 8 ?
                            <div className="Table">
                                {
                                    inst.slider_3.slides.map((cardOne, index) => {
                                        return <div className="CardSliderThree" key={index}
                                                    onMouseOver={() => {
                                                        this.setState({activeState: index})
                                                    }}
                                                    onMouseLeave={() => {
                                                        this.setState({activeState: -1});
                                                    }}
                                                    onClick={() => {
                                                        this.setState({stateFlyWindow: true});
                                                        this.setState({number: index});
                                                    }}>
                                            {cardOne.img !=="" ?  <img src={(cardOne.img)}/> : <div className="NoPhoto"/>}

                                            <h5>{(cardOne.Name[this.props.Data.lang])}</h5>
                                            <div className="MiniInfo">{(cardOne.type[this.props.Data.lang])}</div>
                                        </div>
                                    })
                                }
                            </div> :
                            <Slider {...SettingTwo}>
                                {
                                    inst.slider_3.slides.map((cardOne, index) => {
                                        return <div className="CardSliderThree" key={index}
                                                    onMouseOver={() => {
                                                        this.setState({activeState: index})
                                                    }}
                                                    onMouseLeave={() => {
                                                        this.setState({activeState: -1});
                                                    }}
                                                    onClick={() => {
                                                        this.setState({stateFlyWindow: true});
                                                        this.setState({number: index});
                                                    }}>

                                            {cardOne.img !=="" ?  <img src={(cardOne.img)}/> : <div className="NoPhoto"/>}
                                            <h5>{(cardOne.Name[this.props.Data.lang])}</h5>
                                            <div className="MiniInfo">{(cardOne.type[this.props.Data.lang])}</div>

                                        </div>
                                    })
                                }
                            </Slider>}
                        <div className={classNames('FlyWindow', {'activeFlyWindow':this.state.stateFlyWindow===true && inst.slider_3.slides[this.state.number].hoverTxt[this.props.Data.lang].length!==0})}
                             onClick={() => {
                                 this.setState({stateFlyWindow: false})
                             }}>
                            <div className="WhiteWindow">
                                <div className="WhiteWindow-Left">
                                    {/*<img className="Photo" src={inst.slider_3.slides[this.state.number].img}/>*/}
                                    {inst.slider_3.slides[this.state.number].img !=="" ?  <img className="Photo" src={inst.slider_3.slides[this.state.number].img}/> : <div className="NoPhoto"/>}
                                    <h4 className="Name">
                                        {inst.slider_3.slides[this.state.number].Name[this.props.Data.lang]}
                                    </h4>
                                    <div className="MiniInfo">
                                        {inst.slider_3.slides[this.state.number].type[this.props.Data.lang]}
                                    </div>
                                </div>
                                <div className="WhiteWindow-Right">
                                    {inst.slider_3.slides[this.state.number].hoverTxt[this.props.Data.lang].map((abz,index)=>{
                                        return <p className="Info" key={index}>
                                            {abz}
                                        </p>
                                    })}

                                </div>
                            </div>
                        </div>
                    </div> : null}

                </div>
            </section>
        )
    }
}

const mapStateToProps = (state) => ({
    Data: state.Data,
    TypeInstitution: state.TypeInstitution
});

export default connect(mapStateToProps)(SliderThree);