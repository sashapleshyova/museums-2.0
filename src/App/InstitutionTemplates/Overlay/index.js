import React, {Component} from 'react';
import './style.scss';
import {connect} from 'react-redux';

class Overlay extends Component {

    render() {
        const {Data} = this.props;
        const {inst} = this.props;
        return (

            <section className="Overlay">
                <h2>{inst.mainTitle[this.props.Data.lang]}</h2>
                {inst.mainImg!=="" ? <img src={(inst.mainImg)}/>: null}
            </section>
        )
    }
}

const mapStateToProps = (state) => ({
    Data: state.Data,
    TypeInstitution: state.TypeInstitution
});

export default connect(mapStateToProps)(Overlay);