import React, {Component} from 'react';
import './style.scss';
import {connect} from 'react-redux';
import Slider from 'react-slick';
import classNames from "classnames";

let Setting = {
    dots: true,
    dotsClass: 'responsive',
    infinite: false,
    speed: 500,
    initialSlide: 0,
    slidesToShow: 6,
    slidesToScroll: 6,
    autoplay: false,
};

class Rewards extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activeState: -1,
            choicePhoto:-1
        };
    }

    render() {

        const {Data} = this.props;
        const {inst} = this.props;
        return (
            <section className="Rewards">
                <div className="Rewards-Content container">


                    {inst.slider_6 !== undefined ? <div>
                        <h4>{(inst.slider_6.title[this.props.Data.lang])}</h4>
                        <Slider {...Setting}>
                            {
                                inst.slider_6.slides.map((cardOne, index) => {
                                    return <div className="CardSliderSix" key={index}
                                                onMouseOver={() => {
                                                    this.setState({activeState: index})
                                                }}
                                                onMouseLeave={() => {

                                                    this.setState({activeState: -1});

                                                }}
                                                onClick={() => {
                                                    this.setState({choicePhoto:index});
                                                    console.log(this.state.choicePhoto)
                                                }}>

                                        {cardOne.img !=="" ?  <img src={(cardOne.img)}/> : <div className="NoPhoto"/>}
                                        <div className="RightInfo">

                                        </div>
                                        {/*<div*/}
                                            {/*className={classNames('HoverWindow', {'activeWindow': this.state.activeState === index && cardOne.hoverTxt[this.props.Data.lang] !== ""})}>{(cardOne.hoverTxt[this.props.Data.lang])}</div>*/}
                                    </div>
                                })
                            }
                        </Slider>
                    </div> : null}



                </div>
                {this.state.choicePhoto !== -1 ?
                    <div className="BackgroundBigPhoto"
                         onClick={() => {
                             this.setState({choicePhoto:-1})
                         }}>
                        <img className="BigPhoto" src={inst.slider_6.slides[this.state.choicePhoto].img} />
                        <div className="Info">
                            <h4>{(inst.slider_6.slides[this.state.choicePhoto].Name[this.props.Data.lang])}</h4>
                            <h5>{(inst.slider_6.slides[this.state.choicePhoto].type[this.props.Data.lang])}</h5>
                            {inst.slider_6.slides[this.state.choicePhoto].hoverTxt[this.props.Data.lang]}

                        </div>
                    </div>
                    : null}
            </section>
        )
    }
}

const mapStateToProps = (state) => ({
    Data: state.Data
});

export default connect(mapStateToProps)(Rewards);