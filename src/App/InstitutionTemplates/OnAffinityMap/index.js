import React, {Component} from 'react';
import './style.scss';
import {connect} from 'react-redux';
import kindergarten from "../../../messages/school";
import {Link} from "react-router-dom";
import {Map, Placemark, Rectangle, YMaps} from "react-yandex-maps";
import icon from "../../Sort/MapType/img/1.png";
import ActionSort from "../../../redux/Sort/actions";
import DataBase from './../../../messages/allInstitution';


class OnAffinityMap extends Component {


    render() {
        const {Data} = this.props;
        const {inst} = this.props;
        const mapState = {center: inst.coordinates, zoom: 14, controls: []};
        return (
            <section className="OnAffinityMap">
                {console.log("yuhgjhg")}
                <div className="OnAffinityMap-Content container">
                    <div className="Line">
                        <h4>{Data.data.onAffinity.yes[inst.type]}</h4>
                        <div className="Map"
                             onClick={() => {
                                 this.props.view();
                             }}>
                            <div className="Image"/>
                        </div>
                    </div>
                    <YMaps>
                        <div id="map-basics">

                            <Map state={mapState} width={1240} height={550}>
                                <Rectangle
                                    geometry={{
                                        coordinates: [[0, 0], [100, 100]],
                                    }}
                                    options={{
                                        fillColor: '#000',
                                        fillOpacity: 0.4,
                                    }}
                                />
                                {inst.schoolNear.map((place, index) => {
                                    return <Placemark key={index}
                                                      geometry={{
                                                          coordinates: DataBase[place].coordinates
                                                      }}
                                                      properties={{
                                                          hintContent: DataBase[place].mainTitle[this.props.Data.lang],
                                                          balloonContent: DataBase[place].mainTitle[this.props.Data.lang]+" "+DataBase[place].address[this.props.Data.lang],
                                                          image:DataBase[place].mainImg,
                                                      }}
                                                      options={{
                                                          iconLayout: 'default#image',
                                                          iconImageHref:icon,
                                                          iconImageSize: [32, 32],
                                                      }}
                                    />
                                })}
                            </Map>
                        </div>
                    </YMaps>
                </div>
            </section>
        )
    }
}

const mapStateToProps = (state) => ({
    Data: state.Data,
    Sort: state.Sort
});
const mapDispatchToProps = (dispatch) => ({
    view: () => dispatch(ActionSort.view()),
});
export default connect(mapStateToProps, mapDispatchToProps)(OnAffinityMap);
