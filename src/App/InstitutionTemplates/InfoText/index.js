import React, {Component} from 'react';
import './style.scss';
import {connect} from 'react-redux';

class InfoText extends Component {

    render() {
        const {Data} = this.props;
        const {inst} = this.props;
        return (
            <section className="InfoText">
                <div className="InfoText-Content container">

                    {
                        inst.text.map((text, index) => {
                            return <div className="CardInfo" key={index}>
                                <h4>{inst.text[index].title[this.props.Data.lang]}</h4>
                                    {text.text[this.props.Data.lang].map((abz, index)=>{
                                        return <p key={index}>{abz}</p>
                                    })}

                            </div>
                        })
                    }

                </div>
            </section>
        )
    }
}

const mapStateToProps = (state) => ({
    Data: state.Data
});

export default connect(mapStateToProps)(InfoText);