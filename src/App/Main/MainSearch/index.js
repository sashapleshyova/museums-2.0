import React, {Component} from 'react';
import './style.scss';
import {connect} from 'react-redux';
import {Link} from "react-router-dom";
import ActionSort from "../../../redux/Sort/actions";

class MainSearch extends Component {


    render() {
        function makeWord() {
            let text = document.getElementsByTagName("input")[0];
            console.log(text.value);
            return text.value;
        }

        const {Data} = this.props;

        return (
            <section className="MainSearch">
                <div className="MainSearch-Content container">
                    <h3 className="MainSearchText">{Data.data.main.mainSearch.mainSearchTitle}</h3>
                    <div className="SearchField">
                        <input
                            placeholder={Data.data.main.mainSearch.mainSearchExample}
                            className="text"/>
                        <Link to="/Sort/all" className="MainSearchIcon" onClick={() => {
                            this.props.word(makeWord());
                        }}>
                            <div className="MainSearchImage"/>
                        </Link>
                    </div>
                </div>
            </section>
        )
    }
}

const mapStateToProps = (state) => ({
    Data:state.Data
});
const mapDispatchToProps = (dispatch) => ({
    word: (word) => dispatch(ActionSort.word(word)),
});
export default connect(mapStateToProps,mapDispatchToProps)(MainSearch);