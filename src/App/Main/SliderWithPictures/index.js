import React, {Component} from 'react';
import './style.scss';
// import './../../../../node_modules/slick-carousel';
import Slider from 'react-slick';
import connect from "react-redux/es/connect/connect";


let setting = {
    dots: true,
    dotsClass: 'autoplay',
    infinite: true,
    speed: 500,
    initialSlide: 0,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 3000,
};
class SliderWithPictures extends Component {

    render() {
        const {Data} = this.props;
        return (
            Data.data.main.main_slider.slides.length===0 ? null :
            <section className="SliderWithPictures">
                <div className="SliderWithPictures-Content container">
                    <Slider {...setting}>{
                        Data.data.main.main_slider.slides.map((slide,index)=>{
                            return <div className="SlideContent" key={index}>
                                <img src={slide.photo}/>
                                    {slide.text==="" ? null :
                                        <div className="Signature"/>
                                    }
                                   <h3>{slide.text}</h3>

                            </div>
                        })
                    }
                    </Slider>
                </div>
            </section>
        )
    }
}
const mapStateToProps = (state) => ({
    Data:state.Data,
});
export default connect(mapStateToProps)(SliderWithPictures);