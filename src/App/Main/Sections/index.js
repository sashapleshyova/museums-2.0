import React, {Component} from 'react';
import './style.scss';
import {connect} from 'react-redux';
import {Link} from "react-router-dom";
import classNames from 'classnames';
import actions from "../../../redux/StateVeteran/actions";
class Sections extends Component{

    render(){
        const {Data} = this.props;
        return(
            <section className="Sections">
                <div className="Sections-Content container">{
                    Data.data.main.sections.links.map((link, index)=>{
                        return <Link  to={link.link} className="SectionCard" key={index}
                                      onClick={() => {
                                          this.props.addVeteran("");
                                          this.props.searchVeteran();
                                      }}
                        >
                            <div className={classNames('SectionPhoto'+index)}/>
                            <div className="NameCategory">{link.name}</div>
                        </Link>
                    })
                }
                </div>
            </section>
        )
    }
}

const mapStateToProps = (state) => ({
    Data:state.Data,
    StateVeteran: state.StateVeteran
});
const mapDispatchToProps = (dispatch) => ({
    addVeteran:(wordVeteran)=> dispatch(actions.addVeteran(wordVeteran)),
    searchVeteran: (resultVeteransMas) => dispatch(actions.searchVeteran(resultVeteransMas)),
});
export default connect(mapStateToProps, mapDispatchToProps)(Sections);
