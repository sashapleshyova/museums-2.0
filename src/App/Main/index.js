import React, {Component} from 'react';
import './style.scss';
import {connect} from 'react-redux';
import AboutProject from './AboutProject';
import MainSearch from './MainSearch';
import Sections from './Sections';
import SliderWithPictures from './SliderWithPictures';
import ActionSort from "../../redux/Sort/actions";

class Main extends Component {
    componentDidMount() {
        window.scrollTo(0,0);
        this.props.setDistricts([0,1,2]);
        this.props.setUp();
        this.props.word("");
        this.props.viewSet();
        this.props.type("all");
    }
    render() {
        return (
            <div className="Main">
                <SliderWithPictures/>
                <MainSearch/>
                <Sections/>
                <AboutProject/>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) => ({
    setDistricts: (district) => dispatch(ActionSort.setDistricts(district)),
    setUp: () => dispatch(ActionSort.setUp()),
    word: (word) => dispatch(ActionSort.word(word)),
    viewSet: () => dispatch(ActionSort.viewSet()),
    type:(type)=>dispatch(ActionSort.type(type)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Main);

