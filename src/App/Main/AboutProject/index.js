import React, {Component} from 'react';
import './style.scss';
import {connect} from 'react-redux';

class AboutProject extends Component {

    render() {
        const {Data} = this.props;
        return (
            <section className="AboutProject">
                <div className="AboutProject-Content container">
                    <div className="AboutProjectImage"/>

                    {
                        Data.data.main.aboutProject.map((item, index) => {
                            return <div className="AboutProjectInfo" key={index}>
                                {index === 0 ?
                                    <h2 className="AboutProjectTitle">
                                        {item.title}
                                    </h2>
                                    :
                                    <h4 className="AboutProjectTitle">
                                        {item.title}
                                    </h4>}

                                {item.text.map((p, indexP) => {
                                    return <p key={indexP}>
                                        {p}
                                    </p>
                                })}

                            </div>
                        })
                    }

                </div>
            </section>
        )
    }
}

const mapStateToProps = (state) => ({
    Data: state.Data,
});

export default connect(mapStateToProps)(AboutProject);