import React, {Component} from 'react';
import './style.scss';
import { connect } from 'react-redux';
import dataActions from '../../../redux/Data/actions';
import actions from '../../../redux/States/actions';

class Footer extends Component{

  render(){

    const {Data} = this.props;

    return(
      <footer className="Footer">
        <div className="Footer-Content container">
          <div className="Footer-Content-Text">
            <p>{Data.data.footer.support}</p>
            <p>{Data.data.footer.TSU}</p>
            <div className="Image"/>
            </div>
          <h3 className="Footer-Content-Title">{Data.data.footer.mainName}</h3>
        </div>
      </footer>
    )
  }
}

const mapStateToProps = (state) => ({
  Data: state.Data
});

export default connect(mapStateToProps, null)(Footer);