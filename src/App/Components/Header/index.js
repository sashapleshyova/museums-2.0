import React, {Component} from 'react';
import './style.scss';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import actions from '../../../redux/States/actions';
import dataActions from '../../../redux/Data/actions';
import ActionSort from "../../../redux/Sort/actions";

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            section: false
        }
    }

    render() {

        const {States} = this.props;
        const {Data} = this.props;

        return (
            States.menuState === false
                ? <header className="Header">
                    <div className="Header-Content container">
                        <Link to="/" className="Header-Content-Title"><h1>{Data.data.header.mainName}</h1></Link>
                        <section className="Header-Content-Right">
                            <button className="SelectLang"
                                    onClick={() => {
                                        this.props.lang();
                                        this.props.addLang();
                                    }}>{Data.lang}</button>
                            <button className="MenuButton"
                                    onClick={() => {
                                        this.props.changeMenuState();
                                        document.body.style.overflow = "hidden"
                                    }}>
                                <div className="line"/>
                                <div className="line"/>
                                <div className="line"/>
                            </button>
                        </section>
                    </div>
                </header>
                : <section className="Menu">
                    <div className="Menu-Content">
                        <h1 className="Menu-Content-Title">{Data.data.header.mainName}</h1>
                        <div className="Line"/>
                        <section className="Links">
                            {
                                (Data.data.header.links).map((link, index) => {
                                    return <div className="LinkBlock" key={index} onClick={() => {
                                       this.props.changeMenuState();
                                        document.body.style.overflow = "auto";
                                        this.props.setDistricts([0,1,2]);
                                        this.props.type(link.link.substr(6));
                                        // console.log( this.props.type(link.link.substr(6)).payload);
                                        // this.props.addType();
                                        this.props.word("");
                                        // this.props.sort();
                                    }}>
                                        {/*{index === 7 ? <div className="OpenList"*/}
                                                            {/*onClick={() => {*/}
                                                                {/*this.setState({section: !this.state.section});*/}
                                                            {/*}}>{link.name}</div> : */}
                                            <Link to={link.link}>{link.name}</Link>
                                         {/*}*/}

                                    </div>
                                })
                            }

                        </section>
                        {/*<div className={classNames('Array', {'NoActive': this.state.section === false})}>*/}
                            {/*{*/}
                                {/*(Data.data.additionalEducation.links).map((link, index) => {*/}
                                    {/*return <div className="MiniLinkBlock" key={index} onClick={() => {*/}
                                        {/*this.props.changeMenuState();*/}
                                        {/*document.body.style.overflow = "auto";*/}
                                    {/*}}>*/}
                                        {/*<Link to={link.link}*/}
                                              {/*onClick={() => {*/}

                                                  {/*this.props.typeKey(link.link.substr(6));*/}
                                                  {/*this.props.addType();*/}
                                              {/*}} >{link.name}</Link>*/}
                                    {/*</div>*/}
                                {/*})*/}
                            {/*}*/}
                        {/*</div>*/}
                    </div>
                    <button className="XButton"
                            onClick={() => {
                                this.props.changeMenuState();
                                document.body.style.overflow = "auto"
                            }}>
                        <div className="XLine1"/>
                        <div className="XLine2"/>
                    </button>
                </section>
        );
    }
}

const mapStateToProps = (state) => ({
    States: state.States,
    Data: state.Data,
});

const mapDispatchToProps = (dispatch) => ({
    lang: () => dispatch(dataActions.lang()),
    changeMenuState: () => dispatch(actions.changeMenuState()),
    addLang: (lang) => dispatch(dataActions.addLang(lang)),
    setDistricts: (district) => dispatch(ActionSort.setDistricts(district)),
    setUp: () => dispatch(ActionSort.setUp()),
    word: (word) => dispatch(ActionSort.word(word)),
    view: () => dispatch(ActionSort.view()),
    type:(type)=>dispatch(ActionSort.type(type)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);