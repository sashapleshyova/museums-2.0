import React, {Component} from 'react';
import './style.scss';
import {connect} from 'react-redux';
import actions from './../../../redux/StateVeteran/actions'

class Search extends Component {


    render() {
        function makeWord() {
            let text = document.getElementsByTagName("input")[0];
            return text.value;
        }
        const {Data} = this.props;
        return (
            <section className="Search">
                <div className="Search-Content container">
                    <h3 className="SearchText">{Data.data.veterans.title}</h3>
                    <div className="SearchField">
                        <input placeholder={Data.data.veterans.placeholder} className="text"/>
                        <div className="SearchIcon" onClick={() => {
                            this.props.addVeteran(makeWord());
                            this.props.searchVeteran();
                        }}>
                            <div className="SearchImage"/>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

const mapStateToProps = (state) => ({
    Data: state.Data,
    StateVeteran: state.StateVeteran
});

const mapDispatchToProps = (dispatch) => ({
    addVeteran:(wordVeteran)=> dispatch(actions.addVeteran(wordVeteran)),
    searchVeteran: (resultVeteransMas) => dispatch(actions.searchVeteran(resultVeteransMas)),
});



export default connect(mapStateToProps,mapDispatchToProps)(Search);