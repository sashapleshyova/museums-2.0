import React, {Component} from 'react';
import './style.scss';
import {connect} from 'react-redux';
import {Link} from "react-router-dom";
import classNames from 'classnames';
import Slider from "react-slick/lib";
import veteransClub from "./../../../messages/veteransClub"

class Gallery extends Component {

    render() {
        const {Data} = this.props;
        let SliderForDay = {
            dots: true,
            dotsClass: 'autoplay',
            infinite: true,
            speed: 500,
            initialSlide: 0,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 3000,
        };
        return (
            <section className="Gallery">
                <div className="Gallery-Content container">
                    <Slider {...SliderForDay}>{
                       veteransClub.veteransSlider.map((photo,index)=>{
                            return <div className="SlideContent" key={index}>
                                <img src={photo}/>
                            </div>
                        })
                    }
                    </Slider>
                </div>
            </section>
        )
    }
}

const mapStateToProps = (state) => ({
    Data: state.Data,
});
const mapDispatchToProps = (dispatch) => ({
});
export default connect(mapStateToProps, mapDispatchToProps)(Gallery);
