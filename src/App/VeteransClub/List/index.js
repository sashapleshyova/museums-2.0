import React, {Component} from 'react';
import './style.scss';
import {connect} from 'react-redux';
import {Link} from "react-router-dom";
import classNames from 'classnames';
import actions from "../../../redux/StateVeteran/actions";
import veterans from "../../../messages/veterans";

class List extends Component {

    render() {
        const {Data} = this.props;
        return (
            <section className="List">
                <div className="List-Content container">
                    {this.props.StateVeteran.wordVeteran.length===0 ?
                        veterans.map((vet,index)=>{
                            return <Link to={"/PageAboutVeteran/"+vet.ID} className="Card" key={index}>
                                {vet.mainPhoto==="" ? <div className="NoPhoto"/> :  <img src={vet.mainPhoto} className="Image"/>}
                                <div className="Name">{vet.name[this.props.Data.lang]}</div>
                                <div className="InfoText">{vet.miniInfo[this.props.Data.lang]}</div>
                            </Link>
                        })

                        :

                        this.props.StateVeteran.resultVeteransMas.map((vet,index)=>{
                            return <Link to={"/PageAboutVeteran/"+vet.ID} className="Card" key={index}>
                                    {vet.mainPhoto==="" ? <div className="NoPhoto"/> :  <img src={vet.mainPhoto} className="Image"/>}
                                <div className="Name">{vet.name[this.props.Data.lang]}</div>
                                <div className="InfoText">{vet.miniInfo[this.props.Data.lang]}</div>
                            </Link>
                        })

                    }

                </div>
            </section>
        )
    }
}

const mapStateToProps = (state) => ({
    Data: state.Data,
    StateVeteran: state.StateVeteran
});
const mapDispatchToProps = (dispatch) => ({
});
export default connect(mapStateToProps, mapDispatchToProps)(List);
