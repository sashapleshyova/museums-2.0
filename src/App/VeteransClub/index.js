import React, {Component} from 'react';
import {connect} from 'react-redux';
import Info from './Info';
import List from './List';
import Gallery from './Gallery';
import Search from './Search';
import ActionSort from "../../redux/Sort/actions";
import actions from "../../redux/StateVeteran/actions";


class VeteransClub extends Component {
    componentDidMount() {
        window.scrollTo(0,0);
        this.props.setDistricts([0,1,2]);
        this.props.setUp();
        this.props.word("");
        this.props.viewSet();
        this.props.type("all");
    }
    render() {

        return (
            <div className="VeteransClub">
                <Gallery/>
                <Info/>
                <Search/>
                <List/>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
});
const mapDispatchToProps = (dispatch) => ({
    setDistricts: (district) => dispatch(ActionSort.setDistricts(district)),
    setUp: () => dispatch(ActionSort.setUp()),
    word: (word) => dispatch(ActionSort.word(word)),
    viewSet: () => dispatch(ActionSort.viewSet()),
    type:(type)=>dispatch(ActionSort.type(type)),

});
export default connect(mapStateToProps, mapDispatchToProps)(VeteransClub);

