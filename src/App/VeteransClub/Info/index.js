import React, {Component} from 'react';
import './style.scss';
import {connect} from 'react-redux';
import {Link} from "react-router-dom";
import classNames from 'classnames';
import actionsType from "../../../redux/StateVeteran/actions";
import veteransClub from "./../../../messages/veteransClub"



class Info extends Component {

    render() {
        const {Data} = this.props;
        return (
            veteransClub.infoAboutVeteransClub.length !==0 ?
            <section className="Info">
                <div className="Info-Content container">
                    <div className={classNames('MainInfo',{'active' : this.props.StateVeteran.open===true})}>
                        {veteransClub.infoAboutVeteransClub.map((inf, index)=>{
                            return  <div className="InfoBlock" key={index}>
                                <h4>{inf.title[this.props.Data.lang]}</h4>
                               {inf.text[this.props.Data.lang].map((abz, indexAbz)=>{
                                    return <p key={indexAbz}>{abz}</p>
                                })}
                            </div>

                        })}

                        {/*<div className="Gradient"*/}
                        {/*style={{*/}
                            {/*background:this.props.StateVeteran.open===true ? 'transparent' : null*/}
                        {/*}}/>*/}
                    </div>

                    {/*<div className="More"*/}
                         {/*onClick={() => {*/}
                             {/*this.props.open(!this.props.StateVeteran.open);*/}
                         {/*}}>{this.props.StateVeteran.open===false ? Data.data.veterans.info.more : Data.data.veterans.info.little}</div>*/}
                </div>
            </section> : null
        )
    }
}

const mapStateToProps = (state) => ({
    Data: state.Data,
    StateVeteran: state.StateVeteran
});
const mapDispatchToProps = (dispatch) => ({
    open: (stateOpen) => dispatch(actionsType.open(stateOpen))
});
export default connect(mapStateToProps, mapDispatchToProps)(Info);
