import React, {Component} from 'react';
import './style.scss';
import {connect} from 'react-redux';
import actions from "../../redux/States/actions";
import Tables from "./../../messages/tables"

class TableLeaders extends Component {
    componentDidMount() {
        window.scrollTo(0, 0);
        this.props.changeNumberTable(this.props.match.params.numInst);
    }

    render() {

        const {Data} = this.props;

        return (

            <div className="TableLeaders">
                <h2>{Tables[3][this.props.States.numberTable][this.props.Data.lang]}</h2>
                <section className="TableLeaders-Content container">
                    {Tables[this.props.States.numberTable][this.props.Data.lang]!==undefined ?
                        Tables[this.props.States.numberTable][this.props.Data.lang].map((item, index) => {
                        return <div key={index}>{item}</div>
                    })
                    :null}
                </section>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    Data: state.Data,
    States: state.States,
});


const mapDispatchToProps = (dispatch) => ({
    changeNumberTable: (numberTable) => dispatch(actions.changeNumberTable(numberTable)),
});

export default connect(mapStateToProps, mapDispatchToProps)(TableLeaders);