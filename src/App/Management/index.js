import React, {Component} from 'react';
import './style.scss';
import {connect} from 'react-redux';
import classNames from 'classnames';
import {Link} from "react-router-dom";
import actions from "../../redux/States/actions";
import ActionSort from "../../redux/Sort/actions";

class Management extends Component {
    componentDidMount() {
        window.scrollTo(0, 0);
        this.props.setDistricts([0, 1, 2]);
        this.props.setUp();
        this.props.word("");
        this.props.viewSet();
        this.props.type("all");

    }

    render() {

        const {Data} = this.props;

        return (
            <div className="Management">
                <section className="Management-Content container">
                    <h2 className="Management-Content-Title">{Data.data.management.title}</h2>
                    <div className={classNames('DropdownList', {'ActiveDropdownList' : this.props.States.dropdownListState===true})}>
                        <div className="OpenLine" onClick={() => {
                            this.props.changeDropdownListState();
                        }}>
                            {Data.data.management.management_in_togliatti}
                            <div className="IconOpen"/>
                        </div>

                        <div className="LinksPage">
                            {Data.data.management.links.map((link, index)=>{
                                return <Link to={link.link} key={index}>{link.name}</Link>
                            })}
                        </div>

                    </div>


                    {
                        Data.data.management.content.map((ras, index) => {
                            return <div className="InfoBlock" key={index}>
                                <h3>{ras.title}</h3>
                                {
                                    ras.text.map((abz, indexAbz) => {
                                        return <p key={indexAbz}>{abz}</p>
                                    })
                                }

                            </div>
                        })
                    }

                </section>
            </div>
        )
    }

}

const mapStateToProps = (state) => ({
    Data: state.Data,
    States: state.States,
});


const mapDispatchToProps = (dispatch) => ({
    changeDropdownListState: () => dispatch(actions.changeDropdownListState()),
    setDistricts: (district) => dispatch(ActionSort.setDistricts(district)),
    setUp: () => dispatch(ActionSort.setUp()),
    word: (word) => dispatch(ActionSort.word(word)),
    viewSet: () => dispatch(ActionSort.viewSet()),
    type: (type) => dispatch(ActionSort.type(type)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Management);