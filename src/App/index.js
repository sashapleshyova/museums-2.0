import React, {Component} from 'react';
import './main.scss'
import {Switch, Route} from 'react-router-dom';
import {hot} from 'react-hot-loader';
import Main from './Main';
import Header from './Components/Header';
import Footer from './Components/Footer';
import History from '../../../museums-2.0/src/App/History';
import Management from './Management';
import Institution from './InstitutionTemplates';
import AdditionalEducation from './AdditionalEducation';
import PageAboutVeteran from './PageAboutVeteran';
import VeteransClub from './VeteransClub';
import Sort from './Sort';
import LeadersOfEducation from './LeadersOfEducation';
import Table from './TableLeaders';
import Documentation from './Documentation'

const App = () => (
    <div className="SiteWrapper">
        <Header/>
        <div className="MainBoxContainer">
            <Switch>
                <Route exact path="/" component={Main}/>
                <Route path="/History" component={History}/>
                <Route path="/Management" component={Management}/>
                <Route path="/Inst/:id" component={Institution}/>
                <Route path="/AdditionalEducation" component={AdditionalEducation}/>
                <Route exact path="/Sort/:id" component={Sort}/>
                <Route path="/PageAboutVeteran/:vet" component={PageAboutVeteran}/>
                <Route path="/VeteransClub" component={VeteransClub}/>
                <Route path="/Table/:numInst" component={Table}/>
                <Route path="/LeadersOfEducation" component={LeadersOfEducation}/>
                <Route path="/Documentation" component={Documentation}/>
            </Switch>
        </div>
        <Footer/>
    </div>
);

export default hot(module)(App);